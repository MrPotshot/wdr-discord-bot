module.exports = function rankEmbed(message, rank) {
    const Discord = require("discord.js");
    const channel = message.member.guild.channels.cache.find(channel => channel.name === "logs");
    if (!channel) return;
    const embed = new Discord.MessageEmbed()
        .setAuthor(`${message.member.user.tag}`, `${message.member.user.displayAvatarURL()}`)
        .setDescription(`${message.member.user.tag} (${message.member.user.id}) changed their rank to ${rank}`)
        .setFooter("Rank Change")
        .setTimestamp()
        .setColor(0x0059B3);
    channel.send({embed});
};