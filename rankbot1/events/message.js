const config = require("../config.json");
const dmDelete = require("../util/dmDelete");
module.exports = message => {

    // Ignore other bots //
    if (message.author.bot) {
        return;
    }
    if (!message.guild) {
        message.author.send(config.errDMBot);
        return;
    }
    // Ranking commands must be in #role-commands //
    if (message.channel.id !== config.sortingHatID) {
        return;
    }
    // ---------------------- PARSING START --------------------- //
    // Commands must start with the defined prefix //
    if (!message.content.startsWith(config.prefix)) {
        dmDelete(message, config.errBadPrefix+"`"+config.prefix+"`");
        return;
    }

    // Tokenize message //
    var messageParts = message.content.toLowerCase().split(" ");

    // Commands must have at least 1 and at most 2 tokens //
    if (messageParts.length < 1) {
        dmDelete(message, config.errBadParse);
        return;
    }
// ---------------------- PARSING END ---------------------- //

    let perms = 99;
    let client = message.client;
    let args = message.content.split(/\s+/g).slice(1);
    let command = message.content.split(" ")[0].slice(config.prefix.length);
    command = command.toLowerCase();
    //console.log(command);
    let cmd;
    if (client.commands.has(command)) {
        cmd = client.commands.get(command);
    } else if (client.aliases.has(command)) {
        cmd = client.commands.get(client.aliases.get(command));
    }
    if (cmd) {
        cmd.run(client, message, args, perms);
    }
    else {
        dmDelete(message, config.errUnknownCmd);
    }






};