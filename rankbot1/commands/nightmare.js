const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {

  var nightmare = message.guild.roles.cache.get(config.nightmare);
  let nonraider = message.guild.roles.cache.get(config.nonraider);
  if (!message.member.roles.cache.has(nightmare.id)) {
    if(message.member.roles.cache.has(nonraider.id)){
      message.member.roles.remove(nonraider).catch(console.error);
    }
    otherEmbed(message, "Nightmare", "added");
    message.member.roles.add(nightmare).catch(console.error);
    dmDelete(message, "**Nightmare** " + config.notifRoleAdd);
  } else {
    otherEmbed(message, "Nightmare", "removed");
    message.member.roles.remove(nightmare).catch(console.error);
    dmDelete(message, "**Nightmare** " + config.notifRoleRem);
  }
};
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["nm"],
  permLevel: 0
};

exports.help = {
  name: "nightmare",
  description: "assigns the nightmare role",
  usage: "nightmare"
};