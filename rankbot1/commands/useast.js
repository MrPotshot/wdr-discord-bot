const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeLoc = require("../util/removeLoc");
const config = require("../config.json");
exports.run = function(client, message, messageArg){
    var useast = message.guild.roles.cache.get(config.useast);
    if (message.member.roles.cache.has(useast.id)) {
        removeLoc(message);
        dmDelete(message, config.notifLocRem);
        return;
    }
    removeLoc(message);
    message.member.roles.add(useast).catch(console.error);
    otherEmbed(message, "useast", "changed region to");
    dmDelete(message, config.notifLocMod);
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "useast",
    description: "assigns the useast role",
    usage: "useast"
};