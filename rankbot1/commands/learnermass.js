const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {

    var lrnmass = message.guild.roles.cache.get(config.lrnmass);
    var tierRequirement = "Tier 1";
    if (!message.member.roles.cache.has(lrnmass.id)) {
        if (message.member.roles.cache.has(config.cox1) || message.member.roles.cache.has(config.cox2) || message.member.roles.cache.has(config.cox3) || message.member.roles.cache.has(config.cox4) || message.member.roles.cache.has(config.cox5)) {
            otherEmbed(message, "Learner Mass", "added");
            message.member.roles.add(lrnmass).catch(console.error);
            dmDelete(message, "**Learner Mass** " + config.notifRoleAdd);
        } else {
            dmDelete(message, config.errKCLow + "**" + tierRequirement + "**");
        }
    } else {
        otherEmbed(message, "Mass", "removed");
        message.member.roles.remove(lrnmass).catch(console.error);
        dmDelete(message, "**Learner Mass** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "learnermass",
    description: "assigns the learnermass role",
    usage: "learnermass"
};