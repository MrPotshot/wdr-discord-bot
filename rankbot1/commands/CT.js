const dmDelete = require("../util/dmDelete");

exports.run = function(client, message, messageArg){
    var cox_mentor = message.guild.roles.cache.get("520749698424766475");
    var ct = message.guild.roles.cache.get("701398336036536352");

    if (message.member.roles.cache.has(cox_mentor.id)) {
        if(!message.member.roles.cache.has(ct.id)){
            message.member.roles.add(ct).catch(console.error);
            dmDelete(message, "CoX Trialer role added\n use !ct command in <#521409656489377813> again to remove");
        }else{
            message.member.roles.remove(ct).catch(console.error);
            dmDelete(message, "CoX Trialer role removed\n you will no longer be pinged for CoX trials");
        }
        return;
    }else if(message.member.roles.cache.has(ct.id)){
        message.member.roles.remove(ct).catch(console.error);
        dmDelete(message, "CoX Trialer role removed\n you will no longer be pinged for CoX trials");
    }else{
        message.author.send("You must be a CoX mentor to assign this role")
		    message.delete();
    }

};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["ct"],
    permLevel: 1
};

exports.help = {
    name: "CT",
    description: "CoX Trialer role",
    usage: "CT"
};
