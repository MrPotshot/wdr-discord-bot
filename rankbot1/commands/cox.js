const dmDelete = require("../util/dmDelete");
const rankEmbed = require("../util/rankEmbed");
const removeCox = require("../util/removeCox");
const config = require("../config.json");

exports.run = function(client, message, messageArg){


    // KC is not a number error //
    if (isNaN(messageArg) || messageArg === "") {
        dmDelete(message, config.errArgNaN);
        return;
    }

    // KC cannot be negative error //
    if (messageArg < 0) {
        dmDelete(message, config.errKcNegative);
        return;
    }

    var cox0 = message.guild.roles.cache.get(config.cox0);
    var cox1 = message.guild.roles.cache.get(config.cox1);
    var cox2 = message.guild.roles.cache.get(config.cox2);
    var cox3 = message.guild.roles.cache.get(config.cox3);
    var cox4 = message.guild.roles.cache.get(config.cox4);
    var cox5 = message.guild.roles.cache.get(config.cox5);
	var coxlearner = message.guild.roles.cache.get(config.coxlearner);
    var diary = message.guild.roles.cache.get(config.diary);
  
    if(message.member.roles.cache.has(diary.id)){
        message.member.roles.remove(diary).catch(console.error);
    }
    
    const  errNoChange = config.errNoChange;
    // Chambers of Xeric: Tier 0 //
    if (messageArg < 5) {
        var reply = config.notifCoxKC + " You are now **Tier 0 (New)**.";
        if (message.member.roles.cache.has(cox0.id)) {
            dmDelete(message, errNoChange[0] + "Tier 0" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 0");
        removeCox(message);
        message.member.roles.add(cox0).catch(console.error);
		message.member.roles.add(coxlearner).catch(console.error);
        dmDelete(message, reply);
        return;
    }

    // Chambers of Xeric: Tier 1 //
    if (messageArg < 25) {
        if (message.member.roles.cache.has(cox1.id)) {
            dmDelete(message, errNoChange[0] + "Tier 1" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 1");
        removeCox(message);
        message.member.roles.add(cox1).catch(console.error);
		message.member.roles.add(coxlearner).catch(console.error);
        dmDelete(message, config.notifCoxKC + " You are now **Tier 1 (Beginner)**.");
        return;
    }

    // Chambers of Xeric: Tier 2 //
    if (messageArg < 75) {
        if (message.member.roles.cache.has(cox2.id)) {
            dmDelete(message, errNoChange[0] + "Tier 2" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 2");
        removeCox(message);
        message.member.roles.add(cox2).catch(console.error);
		message.member.roles.remove(coxlearner).catch(console.error);
        dmDelete(message, config.notifCoxKC + " You are now **Tier 2 (Intermediate)**.");
        return;
    }

    // Chambers of Xeric: Tier 3 //
    if (messageArg < 150) {
        if (message.member.roles.cache.has(cox3.id)) {
            dmDelete(message, errNoChange[0] + "Tier 3" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 3");
        removeCox(message);
        message.member.roles.add(cox3).catch(console.error);
		message.member.roles.remove(coxlearner).catch(console.error);
        dmDelete(message, config.notifCoxKC + " You are now **Tier 3 (Proficient)**.");
        return;
    }

    // Chambers of Xeric: Tier 4 //
    if (messageArg < 500) {
        if (message.member.roles.cache.has(cox4.id)) {
            dmDelete(message, errNoChange[0] + "Tier 4" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 4");
        removeCox(message);
        message.member.roles.add(cox4).catch(console.error);
		message.member.roles.remove(coxlearner).catch(console.error);
        dmDelete(message, config.notifCoxKC + " You are now **Tier 4 (Advanced)**.");
        return;

    }

    // Chambers of Xeric: Tier 5 //
    if (messageArg >= 500) {
        if (message.member.roles.cache.has(cox5.id)) {
            dmDelete(message, errNoChange[0] + "Tier 5" + errNoChange[1]);
            return;
        }
        rankEmbed(message,"Chambers of Xeric Tier 5");
        removeCox(message);
        message.member.roles.add(cox5).catch(console.error);
		message.member.roles.remove(coxlearner).catch(console.error);
        dmDelete(message, config.notifCoxKC + " You are now **Tier 5 (Elite)**.");
    }
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "cox",
    description: "assigns the cox roles",
    usage: "cox"
};
