const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeLoc = require("../util/removeLoc");
const config = require("../config.json");
exports.run = function(client, message, messageArg){
    var oceania = message.guild.roles.cache.get(config.oceania);
    if (message.member.roles.cache.has(oceania.id)) {
        removeLoc(message);
        dmDelete(message, config.notifLocRem);
        return;
    }
    removeLoc(message);
    message.member.roles.add(oceania).catch(console.error);
    otherEmbed(message, "oceania", "changed region to");
    dmDelete(message, config.notifLocMod);
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "oceania",
    description: "assigns the oceania role",
    usage: "oceania"
};