const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var coxlearner = message.guild.roles.cache.get(config.coxlearner);
    if (!message.member.roles.cache.has(coxlearner.id)) {
        let role0 = message.guild.roles.cache.get(config.cox0);
        let role1 = message.guild.roles.cache.get(config.cox1);
        let role2 = message.guild.roles.cache.get(config.cox2);
        let role3 = message.guild.roles.cache.get(config.cox3);
        let role4 = message.guild.roles.cache.get(config.cox4);
        let role5 = message.guild.roles.cache.get(config.cox5);
        let userRoles = message.member.roles.cache;
        if (userRoles.has(role0.id) || userRoles.has(role1.id) || userRoles.has(role2.id) || userRoles.has(role3.id) || userRoles.has(role4.id) || userRoles.has(role5.id)){
            otherEmbed(message, "Cox Learner", "added");
            message.member.roles.add(coxlearner).catch(console.error);
            dmDelete(message, "**Cox Learner** " + config.notifRoleAdd);
        }
        else{
            message.delete();
        }

    } else {
        otherEmbed(message, "Cox Learner", "removed");
        message.member.roles.remove(coxlearner).catch(console.error);
        dmDelete(message, "**Cox Learner** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "coxlearner",
    description: "assigns the coxlearner role",
    usage: "coxlearner"
};
