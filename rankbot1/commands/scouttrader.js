const dmDelete = require("../util/dmDelete");
const config = require("../config.json");
const otherEmbed = require("../util/otherEmbed");
exports.run = async function(client, message, messageArg){
  var scouttrader = message.guild.roles.cache.get("828750677701558312");

  if(message.member.roles.cache.has(scouttrader.id)){
    otherEmbed(message, "Scout trader", "removed");
    message.member.roles.remove(scouttrader).catch(console.error);
    dmDelete(message, "**Scout trader** " + config.notifRoleRem);
  }
  else {
    let role0 = message.guild.roles.cache.get(config.cox0);
    let role1 = message.guild.roles.cache.get(config.cox1);
    let role2 = message.guild.roles.cache.get(config.cox2);
    let role3 = message.guild.roles.cache.get(config.cox3);
    let role4 = message.guild.roles.cache.get(config.cox4);
    let role5 = message.guild.roles.cache.get(config.cox5);
    let userRoles = message.member.roles.cache;
    if (!(userRoles.has(role0.id) || userRoles.has(role1.id) || userRoles.has(role2.id) || userRoles.has(role3.id) || userRoles.has(role4.id) || userRoles.has(role5.id))) {
      dmDelete(message, "**You need a tier role to be assigned Scout Trader.**\n" +
        "- Make sure you RSN is set via <#500193114506526720>\n" +
        "- Make sure you have set your raids roles by inputting your CoX kc in <#521409656489377813>\n" +
        "- If you are currently a Non-Raider, you will need to type `!modmail I would like to have non-raider removed please\" into <#408272419573202955>` before you can do these steps.");
      return;
    }


      const {google} = require("googleapis"); //npm install googleapis@39 --save
    const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
    const google_client = new google.auth.JWT(
      google_sheet_keys.client_email,
      null,
      google_sheet_keys.private_key,
      ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
    );

    google_client.authorize(function (error, tokens) {
      if (error) {
        console.log(error);
        return;
      }
    });
    const google_sheet = google.sheets({version: "v4", auth: google_client})
    const opt1 = {
      spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
      range: "Scout_trading_bans"
    }
    var data = await google_sheet.spreadsheets.values.get(opt1);
    var bans = data.data.values;

    function exists(arr, search) {
      return arr.some(row => row.includes(search));
    }


    if (exists(bans, message.author.id)) {
      dmDelete(message, "You can't assign this role because you are banned from scout trading");
      return;
    }
    otherEmbed(message, "Scout trader", "added");
    message.member.roles.add(scouttrader).catch(console.error);
    dmDelete(message, "**Scout trader** " + config.notifRoleAdd);
  }
}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["st"],
  permLevel: 1
};

exports.help = {
  name: "scouttrader",
  description: "Scout trader role",
  usage: "scouttrader"
};
