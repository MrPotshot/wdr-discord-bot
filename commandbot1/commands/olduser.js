exports.run = function(client, message, args){
    const connection = client.connection;
    
    switch (args[0]){
        case "search-role":
            search_role(args[1], message)
            break;
        case "search-id":
            search_id(args[1], message)
            break;
        case "add":
            add(args[1], args[2], message)
            break;
        case "remove":
            remove(args[1], args[2], message)
            break;
        case "punish":
            punish(args[1], message);
            break;
    }

    function search_role(role_id, message){
        var sql_command = "SELECT * FROM WDR.OLD_USER_ROLES";
        connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
            if(result == undefined || result == null || result.length == 0){
                return;
            }
            for(var i = 0; i < result.length; i++){
                var msg = "";
                msg = msg + "<@!" + result[i].discord_id + "> : ( ";
                var roles = JSON.parse(result[i].roles);
                for(var j = 0; j < roles.length; j++){
                    msg = msg + "<@&" + roles[j] + "> ";
                }
                msg = msg + ")";
                if(roles.includes(role_id)){
                    message.channel.send(msg);
                }
            }
        })
    }

    function search_id(user_id, message){
        var sql_command = "SELECT * FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + user_id +"\'";
        connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
            if(result == undefined || result == null || result.length == 0){
                message.channel.send(user_id + " not found")
                return;
            }else{
                var msg = "";
                msg = msg + "<@!" + result[0].discord_id + "> : ( ";
                var roles = JSON.parse(result[0].roles);
                for(var j = 0; j < roles.length; j++){
                    msg = msg + "<@&" + roles[j] + "> ";
                }
                msg = msg + ")";
                message.channel.send(msg);
            }
        })
    }

    function add(user_id, role_id, message){
        var sql_command = "SELECT * FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + user_id +"\'";
        connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
            if(result == undefined || result == null || result.length == 0){
                message.channel.send(user_id + " not found")
                return;
            }else{
                var roles = JSON.parse(result[0].roles);
                roles.push(role_id);
                var sql_command_2 = "UPDATE WDR.OLD_USER_ROLES SET roles=\'" + JSON.stringify(roles) + "\' WHERE discord_id =\'" + user_id + "\'";
                connection.query(sql_command_2, function(error, result, fields){
                    if(error){ throw error }
                });
                var msg = "Added <@&"+role_id+"> to <@!"+user_id+">, current roles: \n";
                for(var j = 0; j < roles[j]; j++){
                    msg = msg + "<@&"+roles[j]+">, ";
                }
                message.channel.send(msg)
            }
        })
    }

    function punish(user_id, message){
        var sql_command = "SELECT * FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + user_id +"\'";
        connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
            if(result == undefined || result == null || result.length == 0){
                message.channel.send(user_id + " not found")
                return;
            }else{
                let role_list = "";
                var roles = JSON.parse(result[0].roles);
                for(let role of roles){
                    if(role != "408082929940430858")
                        role_list += "<@&"+role+">";
                }
                roles = ["408082929940430858", "408221074833145863"]
                var sql_command_2 = "UPDATE WDR.OLD_USER_ROLES SET roles=\'" + JSON.stringify(roles) + "\' WHERE discord_id =\'" + user_id + "\'";
                connection.query(sql_command_2, function(error, result, fields){
                    if(error){ throw error }
                });

                var msg = "Removed "+role_list+"from <@!"+user_id+">\n";
                message.channel.send(msg)
            }
        })
    }

    function remove(user_id, role_id, message){
        var sql_command = "SELECT * FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + user_id +"\'";
        connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
            if(result == undefined || result == null || result.length == 0){
                message.channel.send(user_id + " not found")
                return;
            }else{
                var roles = JSON.parse(result[0].roles);
                for( var i = 0; i < roles.length; i++){ 
                    if (roles[i] == role_id) {
                        roles.splice(i, 1); 
                        i--;
                    }
                }                
                var sql_command_2 = "UPDATE WDR.OLD_USER_ROLES SET roles=\'" + JSON.stringify(roles) + "\' WHERE discord_id =\'" + user_id + "\'";
                    connection.query(sql_command_2, function(error, result, fields){
                        if(error){ throw error }
                    });
                var msg = "Removed <@&"+role_id+"> from <@!"+user_id+">, current roles: \n";
                for(var j = 0; j < roles[j]; j++){
                    msg = msg + "<@&"+roles[j]+">, ";
                }
                message.channel.send(msg)
            }
        })
    }
   

};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.MOD
};

exports.help = {
  name: 'old-user',
  description: 'look up/modify data about old users',
  usage: 'search-role role-id\nsearch-id user-id\nadd user-id role-id\nremove user-id role-id'
};