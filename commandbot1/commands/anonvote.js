const emojiPatterns = require ('emoji-patterns');
exports.run = async function(client, message, args){
  const connection = client.connection;
  let argsString = args.join(" ");
  let argsSeperatedCorrectly = argsString.split("|");

  let emoji = [];


  if(argsSeperatedCorrectly.length !== 2){
    await message.channel.send("Wrong amount of arguments supplied")
    return;
  }
  let emojiString = argsSeperatedCorrectly[0];
  let messageId = argsSeperatedCorrectly[1];

  let exit = false;

  let sql_command_check = "SELECT message_id FROM WDR.voteMessages WHERE message_id = ?";
  connection.query(sql_command_check, [messageId],async function(error, result, fields) {
    if (error) {
      throw error
    }
    if(result.length !== 0)
      exit = true;

  });

  if(exit){
    await message.channel.send("this vote already exists");
  }

  let possibleChannels = ["567871852572049429", "471165593605898250", "555830683935965184"]; //doing this so I don't have to loop everywhere
  let messageObject;
  for(let channelId of possibleChannels){
    let channel = message.guild.channels.resolve(channelId);
    if(channel.type !== "text") {
      message.channel.send("something went wrong fetching the channel.");
      return;
    }
    messageObject = await channel.messages.fetch(messageId);
    if(messageObject !== null && messageObject !== undefined){
      break;
    }
  }
  if(messageObject === null || messageObject === undefined) {
    message.channel.send("something went wrong fetching the message");
    return;
  }

  let emojiAllRegex = new RegExp(emojiPatterns["Emoji_All"], 'gu')

  let tempEmoji = [...emojiString.matchAll(emojiAllRegex)];

  for(let tmp of tempEmoji){
    if(isNaN(tmp)) {
      emoji.push(tmp[0]);
    }
  }

  let guildEmojiRegex = /<?a?:?\w{2,32}:(\d{17,19})>?/g

  let guildEmoji = [...emojiString.matchAll(guildEmojiRegex)];

  for(let emote of guildEmoji){
    let resolvedEmote = message.guild.emojis.resolve(emote[1]);
    if(resolvedEmote === undefined || resolvedEmote === null){
      await message.channel.send("Please use emoji from this server");
      return;
    }
    emoji.push(resolvedEmote);
  }
  emoji.push('🗑️');
  for(let emote of emoji) {
    await messageObject.react(emote);
  }

  let sql_command = "INSERT INTO WDR.voteMessages (message_id, open,channelId) VALUES(?, ?, ?)";
  await connection.query(sql_command, [messageObject.id, 1, messageObject.channel.id]);

  await message.channel.send("anonymous vote created");
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['placeholder'],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'anonvote',
  description: 'sets up anonymous votes',
  usage: 'anonvote | votes | messageid'
};
