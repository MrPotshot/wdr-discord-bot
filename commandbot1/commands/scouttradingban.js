exports.run = async function(client, message, args) {
  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well

  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  google_client.authorize(function(error, tokens){
    if(error){
      console.log(error);
      return;
    }
  });
  const google_sheet = google.sheets({version:"v4", auth:google_client})
  const opt1= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Scout_trading_bans"
  }
  var data = await google_sheet.spreadsheets.values.get(opt1);
  var bans = data.data.values;
  //above is sheets stuff


  await message.guild.members.fetch();
  let user = client.users.resolve(args[0]);
  var user_ids;
  if(user == null){
    let users = message.mentions.members;
    user_ids = Array.from(users.keys());
  }
  else{
    user_ids = [user.id];
  }
  if(user_ids.length > 1 || user_ids.length === 0){
    message.channel.send("Please ping exactly 1 user to ban from scout trading. If you believe this is an error please double check they're still in wdr");
    return;
  }
  var userId = user_ids[0];

  var userObject = (message.guild.members.cache.get(userId));

  if(userObject.roles.cache.has("828750677701558312")){
    userObject.roles.remove("828750677701558312");
  }

  message.channel.send("<@!"+userId+"> has been banned from <#530829059048079360>");

  let offences = message.guild.channels.cache.find(channel => channel.id === "408119551566282752");

  let offence = "Bad scouts in scout trading";
  let punishment = "Scout trading ban";
  let totalStrikes = "N/A";

  let msg = "**Discord Name:** <@!"+userId+">\n";//single line break
  msg += "**Discord ID:** "+userId+"\n\n";//Double line break here
  msg += "**Offence:** "+offence+"\n\n"; //Another double line break required here
  msg += "**Punishment:** "+punishment+"\n"; //Single line break
  msg += "**Total Strikes:** "+totalStrikes+"\n"; //One more line break
  msg += "**Issued by:** <@!"+message.member.id+">\n";
  msg += "━━━━━━━━━━━━━━━━━━";

  offences.send(msg);

  let name = "";
  if(args[1] !== undefined){
    name = args[1]
  }
  else{
    let userForName = await message.guild.members.fetch(userId);
    name = userForName.displayName;
  }
  var Data = [];
  Data[0] = [];
  Data[0][0] = userId;
  Data[0][1] = name;
  const opt2= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Scout_trading_bans!" + (bans.length+1)+":"+(bans.length+1),
    valueInputOption: "USER_ENTERED",
    resource: {values: Data}
  }


  var res = await google_sheet.spreadsheets.values.update(opt2);
}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["stb"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'scouttradingban',
  description: 'Bans a user from scout trading',
  usage: 'stb @user'
};
