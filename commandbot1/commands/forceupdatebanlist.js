exports.run = async function(client, message){

  message.channel.send("Updating ban-list");

  const update = require("../util/banlistchannelupdate");
  update(client);
};
  


exports.conf = {
 enabled: true,
 guildOnly: false,
 aliases: ['fubl'],
 permLevel: permLevel.MOD
};

exports.help = {
 name: 'force-update-ban-list',
 description: 'force-update-ban-list',
 usage: 'force-update-ban-list'
};

