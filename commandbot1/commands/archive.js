const settings = require("../settings.json");
const token = settings.token;
const {exec} = require('child_process');
const fs = require('fs');

exports.run = async function(client, message, args) {
  let channel = message.guild.channels.resolve(args[0]);
  if(channel == null){
    if(message.mentions.channels.first()){
      channel = message.mentions.channels.first();
    }
    else if(args.length <= 0){
      channel = message.channel
    }
    else{
      message.channel.send("The channel you added was invalid");
      return;
    }
  }
  if(!channel.name.match(/^(ud-|archive-|cox-|tob-).*/)){
    message.channel.send(`You can't archive ${channel}`);
    return;
  }

  await message.channel.send(`Archiving ${channel} (${channel.id})`);
  var filename = message.guild.name+" - "+channel.name+ " "+channel.id+".html";
  exec("dotnet \"/root/chatexporter/DiscordChatExporter.Cli.dll\" export -t \""+token+"\" " +
    "-b -c "+channel.id+" -o \""+filename+"\"",
    async function(err,stdout,stderr){
      if(err){
        console.log(err);
      }else{
        try {
          await message.channel.send({
            files: [{
              attachment: filename,
              name: filename.replace("\ ", "_")
            }]
          })
          fs.unlink(filename, (err) => {
            if (err) {
              console.error(err)
              return
            }
          })
        }
        catch(e){
          message.channel.send("Ërror: "+e);
        }
      }
  });




}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'archive',
  description: 'archive channels',
  usage: 'archive'
};
