exports.run = async function(client, message, args){
console.log("Test")
    let users = message.mentions.members;
    var user_ids = Array.from(users.keys());
    for(var i = 1; i < args.length; i++){
        if( /^\d+$/.test(args[i]))
            user_ids.push(args[i])
    }
    if(user_ids.length === 0){
        message.channel.send("Please ping at least 1 user to add or remove from the channel");
        return;
    }

  let channel = args[0].trim();
  let update = args[1].trim();
  let channelbln
  if (update === "add"){
    channelbln = true;
} else if (update === "remove"){
    channelbln = false;
} else {
    message.channel.send("Please use add or remove for variables");
    return;}

  let textchannel = message.guild.channels.cache.get(channel);
  if (!textchannel) {
    message.channel.send(`Couldn't find target channel.`);
    return;
  }
  if (textchannel.type != "text") {
    message.channel.send(`Target channel ID must be for a must be a text channel.`);
    return;
  }
  let admin = message.guild.roles.cache.get("408106459910504458");
  if (!textchannel.permissionsFor(message.member).has("VIEW_CHANNEL") && !message.member.roles.cache.has(admin.id)) {
    message.channel.send(`You must be able to view the target channel.`);
    return;
  }

  if(textchannel.id != "521409656489377813" && textchannel.id != "500193114506526720" && textchannel.parentID != "481378650877329417"){
    message.channel.send("You can't change permissions on this channel")
    return;
  }

    for (let i = 0; i < user_ids.length; i++) {
        if(user_ids[i] != channel){
        console.log(user_ids[i]);
        textchannel.createOverwrite(user_ids[i],{ 'VIEW_CHANNEL': channelbln }).then(()=>{
            console.log("added user")});}

    }
   
  
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'channel',
  description: '',
  usage: 'channel channelid [add/remove] @user1 @user2...'
};
