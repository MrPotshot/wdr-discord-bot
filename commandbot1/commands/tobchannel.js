exports.run = async function(client, message, args){
  await message.guild.members.fetch();
  let user = client.users.resolve(args[0]);
  if(user == null){
    user = message.mentions.members.first();
  }else{
    user = message.guild.member(user.id);
  }
  let app_role = message.guild.roles.cache.get("579053677445120015"); //cox-appliacant
  var nickname = user.nickname;
  if(nickname == null || nickname == undefined){//use discord name if no sever nickname
      nickname = message.guild.member(user.id).user.username;
  }

  let perms = [
    { //channel invisable to plebs
      id : message.guild.roles.everyone,
      deny : ['VIEW_CHANNEL']
    },
    {//applicant can see their channel
      id : user.id, 
      allow: ['VIEW_CHANNEL'],
      deny : ['SEND_MESSAGES', 'ADD_REACTIONS']
    },
    {//admin bot
      id : "482771653273714688",
      allow: ['VIEW_CHANNEL']
    },
    {//mod
      id: "408106863117336577",
      allow: ['VIEW_CHANNEL']
    },
    {//tob mentor
      id: "520750027405262858",
      allow: ['VIEW_CHANNEL', 'MANAGE_MESSAGES']
    },
    {//normal mentor
      id: "408107637100511243",
      allow: ['VIEW_CHANNEL', 'MANAGE_MESSAGES']
    },
    {//trial mod
      id: "423653735449886720",
      allow: ['VIEW_CHANNEL']
    }
  ];

  if(!user) return message.delete().then(message.reply('You must have a valid mention.'));
  
  //add role  
  user.roles.add(app_role).catch(console.error);

  //create channel
  let messageId = -1;
  var channel_name = "tob-" + (nickname.replace(/ /g, "-"));
  message.guild.channels.create(channel_name, {type: 'text', parent:'471165503528894464', permissionOverwrites:perms}).then(function (channel){
    channel.send("",{files: ["https://cdn.discordapp.com/attachments/606321358774730772/606321359311732737/rooms_title.png"]}).then(function (message){messageId = message.id})
      .then(() => channel.send("**__Maiden__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Bloat__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Nylocas__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Sotetseg__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Xarpus__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/606321358774730772/606321388302893086/verzik_title.png"]}))
      .then(() => channel.send("**__Speech__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Phase 1__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Phase 2__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("**__Phase 3__**").then(function (message){message.react("❌")}))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/606321358774730772/606321419860705282/wdr_line_red.jpg"]}))
      .then(() => channel.send("<@!" + user.id + ">"))
      .then(() => channel.send("This is the channel that mentors will use to post their trial reviews of you throughout your trial process. There are three steps to this application:\n" +
        "\n" +
        "<:tob:702205226681237505> You will ride along with mentors during mentor raids and observe them.\n" +
        "\n" +
        "<:tob:702205226681237505> You will do a pretrial raid in which you teach and a mentor observes. After this raid, you are eligible for [Trial Mentor] rank should you complete a raid without very serious concerns.\n" +
        "\n" +
        "<:tob:702205226681237505> You will complete trial raids until you successfully pass all rooms including all aspects of Verzik, at which point the mentors will vote on your promotion. You may be asked to complete more trials just to be sure that you are ready for promotion. \n" +
        "\n" +
        "The necessary raids info regarding trialing is found in the Theatre of Blood channel category - we will expect a complete understanding of this material. You are expected to make good progress on your trials throughout each week. If you will be gone for an extended period of time during your trials, please let the mentor team know. *You have two months to complete your trials.*\n" +
        "\n" +
        "You have been given the Applicant role which gives you access to <#440659803841363968>. \n" +
        "\n" +
        "**Please use the `!ping-role TT | (message here)` command to alert the mentors when you are ready to trial/ridealong.** You should aim to trial with as many different mentors as possible to ensure consistency in your feedback. Do not be hesitant to use this to ping the team to let them know you are online and available. Feel free to let any mentors or mods know if you have any questions."))
      .then(() => channel.send("",{files: ["https://cdn.discordapp.com/attachments/606321358774730772/606321419860705282/wdr_line_red.jpg"]}))
      .then(() => channel.messages.fetch(messageId).then(function (message) {message.pin()}));
  });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'tob-channel',
  description: 'Create ToB Applicant channel',
  usage: 'tob-channel'
};
