exports.run = function(client, message, args){
    const connection = client.connection;
  var mes = "";
  var role = "";
  if(args[1] !== undefined && args[1] !== null){
    if(args[0].toLowerCase() == "cox" && args[1].toLowerCase() == "helper" ) {
      role = "cox-helper";
      mes += "CoX helper "
    }else if(args[0].toLowerCase() == "tob" && args[1].toLowerCase() == "helper") {
      role = "tob-helper";
      mes += "ToB helper ";
    }
  }
  if(mes === "") {
    if (args[0].toLowerCase() == "cox") {
      role = "cox";
      mes += "CoX ";
    } else if (args[0].toLowerCase() == "tob") {
      role = "tob";
      mes += "ToB ";
    } else if (args[0].toLowerCase() == "cm") {
      role = "cm";
      mes += "CM "
    } else if (args[0].toLowerCase() == "hm") {
      role = "hm";
      mes += "HM ToB "
    } else if (args[0].toLowerCase() == "nm") {
      role = "nm";
      mes += "Nightmare "
    } else if (args[0].toLowerCase() == "cox-helper") {
      role = "cox-helper";
      mes += "CoX helper "
    } else if (args[0].toLowerCase() == "tob-helper") {
      role = "tob-helper";
      mes += "ToB helper ";
    } else if (args[0].toLowerCase() == "ba") {
      role = "ba";
      mes += "BA "
    } else {
      return;
    }
  }
  var sql_command = "SELECT DISTINCT discord_id FROM WDR.LFR_STAFF WHERE activity=\'" + role + "\'";
  connection.query(sql_command, function(error, result, fields){
    if(error) { throw error }

    for(var i = 0; i < result.length; i++){
      mes += "<@!" + result[i].discord_id + "> ";
    }
    message.channel.send(mes);
  });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['lfrs'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: 'lfr-staff',
  description: '',
  usage: ''
};

