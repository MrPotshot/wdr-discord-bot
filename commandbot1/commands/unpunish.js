const Discord = require('discord.js');
exports.run = async (client, message, args) => {
  message.guild.roles.fetch()
    const connection = client.connection;
  await message.guild.members.fetch();
  let user = client.users.resolve(args[0]);
  if(user == null){
    user = message.mentions.members.first();
  }else{
    user = message.guild.member(user.id);
  }
  if(!user) return message.delete().then(message.reply('You must have a valid mention.'));


  let punished = message.guild.roles.cache.find(role => role.name === "Punished");


  if(!user.roles.cache.has(punished.id)){
    return message.delete().then(message.reply('The user you are trying to unpunish is not punished'));
  }

  var sql_command_1 = "SELECT * FROM WDR.PUNISH WHERE discord_id=\'" + user.id + "\' AND time_unpunished = \"\" ORDER BY time_punished DESC LIMIT 1";
  connection.query(sql_command_1, function(error, result, fields){
    if(error) { throw error }
    if(result == undefined || result == null || result.length == 0){
      return;
    }
    var roles = JSON.parse(result[0].roles);
    user.roles.remove(punished);
    for(var i = 0; i < roles.length; i++){
      if(roles[i] == message.guild.roles.everyone || roles[i].id == 585528815229599775 || roles[i].id == 408247451217166336 || roles[i].id == 408247454207705088){ //check if the role is @everyone, nitro boost, or 3/4
        continue;
      }
      var role = message.guild.roles.cache.get(roles[i]);

      if(role != undefined && role != null){
        user.roles.add(role)
      }
    }
    sql_command_2 = "UPDATE WDR.PUNISH SET time_unpunished=\'"+new Date().toISOString().slice(0, 19).replace('T', ' ')+"\' WHERE discord_id=\'" + user.id + "\' AND time_unpunished = \"\" ORDER BY time_punished DESC LIMIT 1 ";
    message.channel.send(`${user} (${user.id}) unpunished`);

    let modlog = client.channels.cache.find(channel => channel.id == "408119551566282752");
    const embed = new Discord.MessageEmbed()
      .setAuthor(`${message.author.tag}`,`${message.author.displayAvatarURL()}`)
      .setTimestamp()
      .setColor(0x206694)
      .setDescription(`**Action:** Unpunished`+
        `\n**User:** ${user}`+
        `\n**User Id:** ${user.id}`+
        `\n**Moderator:** ${message.author}`);
    client.channels.cache.get(modlog.id).send({embed});

    connection.query(sql_command_2, function(error, result, fields){
      if(error) { throw error }
    })
  });

}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "unpunish",
  description: "Unpunishes the user mentioned",
  usage: "unpunish [mention]"
};
