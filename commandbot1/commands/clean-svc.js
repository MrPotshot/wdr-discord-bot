const fs = require('fs');
exports.run = function(client, message) {
  let contents = "{}";
  try {
    if (fs.existsSync("vcs.json")){
      contents = fs.readFileSync("vcs.json", "utf-8");
    }
  }
  catch (e){
    console.log(e);
  }
  let json = JSON.parse(contents);
  let elements = json.length;
  let keysToRemove= [];
  for(let i = 0; i < elements; i++){
    if(json[i].expiry <= Date.now())
      keysToRemove.push(i);
  }
  while(keysToRemove.length) {
    json.splice(keysToRemove.pop(), 1);
  }

  fs.writeFile('vcs.json', JSON.stringify(json) , (err) => {
    if (err) {
      throw err
    }
  });
  message.channel.send("Voicechats cleaned.");

}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["csvc"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "clean-svc",
  description: "Removes expired saved voice channels",
  usage: 'USAGE: !clean-svc'
};
