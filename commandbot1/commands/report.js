exports.run = function(client, message){
     message.channel.send("Check your DM's!");
     message.author.send({embed: {
    color: 283680,
    fields: [{
        name: "Report Another WDR Member",
        value: "**Users must be from WDR to be reported, unless you are reporting them for scamming and have good evidence.**\n\n**If you would like to report someone, please go to #bot-commands and type !modmail, followed by:**\n<:cox:651156619824070687> Their Discord Name/ID\n<:cox:651156619824070687> Their RSN\n<:cox:651156619824070687> A short explanation of the issue\n<:cox:651156619824070687> Any relevant images of proof (these need to be links e.g. an imgur link)\n<:cox:651156619824070687> If applicable, please provide the Discord Names and RSNs of any others involved (e.g. teammates)\n\n*Please remember that we cannot take action unless there is evidence. We may not be able to help you if you cannot provide that proof.*\n\nThank you for your report, it helps to make WDR a better community!"
      },
    ],
    timestamp: new Date(),
    footer: {
      text: "WDR Bot"
    }
  }
});
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['r'],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'report',
  description: 'DM reporting information.',
  usage: 'report'
};
