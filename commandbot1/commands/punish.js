const ms = require('ms');
const moment = require('moment');
const Discord = require('discord.js');
exports.run = async (client, message, args) => {
  message.guild.roles.fetch()
  const connection = client.connection

  if (!client.lockit) client.lockit = [];
  let modlog = client.channels.cache.find(channel => channel.id == "408119551566282752");
  let reason = args.slice(1).join(" ");
  await message.guild.members.fetch();

  let user = message.guild.members.resolve(args[0])
  let user1 = client.users.resolve(args[0]);

  if(user1 == null || user == null){
    user = message.mentions.members.first();
    user1 = message.mentions.users.first();
  }
  if(!user) return message.delete().then(message.reply('You must have a valid mention.'));
  if(!reason) reason = "No reason provided";
  await message.guild.roles.fetch();
  let punished = message.guild.roles.cache.get("408221074833145863");
  let trial_mod_role = message.guild.roles.cache.get("423653735449886720");
  let mod_role = message.guild.roles.cache.get("408106863117336577");
  let bot_dev = message.guild.roles.cache.get("637279237841354754")
  if(user.roles.cache.has(trial_mod_role.id) || user.roles.cache.has(mod_role.id) || user.roles.cache.has(bot_dev.id)){
    message.channel.send("You can't punish a trial mod or higher");
    return;
  }


    const embed = new Discord.MessageEmbed()
  .setAuthor(`${message.author.tag}`,`${message.author.displayAvatarURL()}`)
  .setTimestamp()
  .setColor(0x00AE86)
  .setDescription(`**Action:** Punishment`+
  `\n**User:** ${user1}`+
  `\n**User Id:** ${user1.id}`+
  `\n**Reason:** ${reason}`+
  `\n**User Roles:** ${user.roles.cache.array().join(" ")}`+
  `\n**Moderator:** ${message.author}`);
    client.channels.cache.get(modlog.id).send({embed});

    let roles = user.roles.cache;

    rolesJson = JSON.stringify(Array.from(roles.keys()))

    var sql_command = "INSERT INTO WDR.PUNISH(discord_id, reason, roles) VALUES(?, ?, ?)"
    await connection.query(sql_command, [user1.id, reason, rolesJson] , function(error, result, fields){
      if(error){ throw error }
    })

  user.roles.cache.forEach(role => {
    if(role.id == 585528815229599775 || role.id == 408247451217166336 || role.id == 408247454207705088 || role.id == message.guild.roles.everyone){
      return;
    }
    user.roles.remove(role);
  })
  user.roles.add(punished.id).catch(console.error)
  message.delete();
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "punish",
  description: "Gives mentioned the punish role for a specific reason",
  usage: "punish [mention] [reason]"
};
