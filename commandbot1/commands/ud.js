exports.run = async function(client, message, args){

    let users = message.mentions.members;
    let last_index = 0;
    let index_set = false;
    var user_ids = Array.from(users.keys());
    for(var i = 1; i < args.length; i++){
        if(/^(\d{17,19})$/g.test(args[i])) {
          if(!index_set) {
            last_index = i - 1;
            index_set = true;
          }
          user_ids.push(args[i])
        }
        if( /^<@!?(\d{17,19})>$/.test(args[i])){
          if(!index_set) {
            last_index = i - 1;
            index_set = true;
          }
        }
    }
    if(user_ids.length === 0){
        message.channel.send("Please ping at least 1 user to have a UD with");
        return;
    }
    let perms = [
        {//mod
            id: "408106863117336577",
            allow: ['VIEW_CHANNEL'],
            type: "role"
        },
        {//trial mod
            id : "423653735449886720",
            allow : ['VIEW_CHANNEL'],
            type: "role"
        },
        {//everyone
            id : message.guild.roles.everyone.id,
            deny : ['VIEW_CHANNEL'],
            type: "role"
        },
        {//admin bots
            id : "482771653273714688",
            allow : ['VIEW_CHANNEL'],
            type: "role"
        },
        {//Twisty
            id : "228019028755611648",
            allow : ['VIEW_CHANNEL'],
            type: "member"
        }
        ];
    for(var i = 0; i < user_ids.length; i++){//mentions
        try {
            await message.guild.members.fetch(user_ids[i]);
            perms.push({id: user_ids[i], allow: ['VIEW_CHANNEL'], type: "member"})
        }
        catch (e){
            console.log(e);
        }
    }
    let channelName = "";
    for(let x = 0; x <= last_index; x++){
      channelName += args[x]+" ";
    }

    channelName =channelName.trim();
    channelName = channelName.replace(/ /g, "-");

    if(!channelName.startsWith("ud-"))
      channelName = "ud-"+channelName;
  try {
    let channel = await message.guild.channels.create(channelName, {
      type: 'text',
      parent:'481378650877329417',
      permissionOverwrites: perms
    });
    message.channel.send(channel.toString() + " created")
  }
  catch(e){
    console.log(e.name+":"+e.message);
    console.log(e.stack);
    console.log(e);
  }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'ud',
  description: '',
  usage: 'ud channel_name @user1 @user2 ...'
};
