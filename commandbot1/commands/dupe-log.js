exports.run = function(client, message, args) {

  //offences channel
  let dupeNames = message.guild.channels.cache.find(channel => channel.id == "800463317583265843");

  //putting arguments in a different array
  let argsString = args.join(" ");
  let argsSeperatedCorrectly = argsString.split("|");

  //checking argument count
  if(argsSeperatedCorrectly.length < 4){
    message.channel.send("Not enough arguments provided")
    return;
  }
  if(argsSeperatedCorrectly.length > 4){
    message.channel.send("Too many arguments provided")
    return;
  }

  //these will always be the same order
  let userId =  argsSeperatedCorrectly[0].trim();
  let UserTwoArray = argsSeperatedCorrectly[1].trim().split(" ");
  let userTwoText = "";
  UserTwoArray.forEach(user =>{
    userTwoText += "<@!"+user+"> - "+user+"\n";
  })
  let rsn = argsSeperatedCorrectly[2].trim();
  let gif = argsSeperatedCorrectly[3].trim();

  //making message here
  let msg = "**Discord Name:** <@!"+userId+">\n";//single line break
  msg += "**Discord ID:** "+userId+"\n";//single line break here
  msg += "**RSN Verified:** `"+rsn+"`\n\n"; //Double line break required here
  msg += "**GIF/Mobile Image:**\n <"+gif+">\n\n"; //Double line break
  msg += "**Duplicate Accounts:** "+userTwoText; //One more line break
  msg += "**Issued by:** <@!"+message.member.id+">\n";
  msg += "━━━━━━━━━━━━━━━━━━";

  dupeNames.send(msg);
  message.delete();
}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'dupe-log',
  description: '',
  usage: 'dupe-log account1 | account2 | rsn | link'
};