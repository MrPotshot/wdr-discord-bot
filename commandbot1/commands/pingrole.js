exports.run = function(client, message, args){

	let cox_app = message.guild.roles.cache.get("572481959083638815");
	let tob_app = message.guild.roles.cache.get("579053677445120015");
    let mentor = message.guild.roles.cache.get("408107637100511243");
    let inactive = message.guild.roles.cache.get("408675904110198795");
    let retire = message.guild.roles.cache.get("676854481933172771");
    let dev = message.guild.roles.cache.get("637279237841354754");
    let mod = message.guild.roles.cache.get("408106863117336577");
    let admin = message.guild.roles.cache.get("408106459910504458");

	if( (!message.member.roles.cache.has(mentor.id) && !message.member.roles.cache.has(inactive) && !message.member.roles.cache.has(retire.id) && !message.member.roles.cache.has(dev.id) && !message.member.roles.cache.has(mod.id) && !message.member.roles.cache.has(admin.id)) && (message.member.roles.cache.has(cox_app.id) || message.member.roles.cache.has(tob_app.id)) && message.channel.id != "440659803841363968"){
		return;
	}
   
	const emoji = message.guild.emojis.cache.find(emoji => emoji.name === 'pingreee');
 
    function ping_roles(roles, index, message_text, message_obj){
	    if(index == roles.length){//all roles made pingable
		    message_obj.channel.send(message_text).then(function (message){message.channel.updateOverwrite(message.author,{ 'ADD_REACTIONS': true }).then(()=>{message.react(emoji)})}).then(()=>{
			    for(var i = 0; i < roles.length; i ++){
				    roles[i].setMentionable(false);
			    }
		    });
	    }else{//make next role in array pingable
		    roles[index].setMentionable(true).then(()=>{
			    ping_roles(roles, index + 1, message_text, message_obj);
		    })
	    }
    }
    
    var str = args.join(" ");
    var args = str.split("|");
    var roles = [];
	var output_string = `${message.author}`;
	if(args.length < 2){
		message.channel.send("Invalid Usage");
	}else{
		var msg = args[args.length-1];
		for(var i = 0; i < args.length-1; i++){
			var role_name = args[i].trim();
			let role =  message.guild.roles.cache.find(role => role.name.toUpperCase() === role_name.toUpperCase()); 
			if(role == undefined || role == null){//invalid role/role not found
			 	message.channel.send(role_name + " Invalid");
			 }else{
			 	output_string = output_string +" <@&"+ role.id + ">"; //adds ping for that role to message
			 	if(role.mentionable == false){ //stores roles to be made unpingable after ping
			 		roles.push(role);
			 	}
			 }
		}
		output_string = output_string + ` ${msg}`;
		ping_roles(roles, 0, output_string, message);
	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMENTOR
};

exports.help = {
  name: 'ping-role',
  description: 'Pings a role (or multiple) with a message',
  usage: '\nping-role ROLE | message\nping-role ROLE1 | ROLE2 | ... | message'
};
