const fs = require('fs');

exports.run = function(client, message){

	var text = "WE DO RAIDS STAFF\n";

    var mentor_set = keys(message.guild.roles.cache.get("408107637100511243").members);
    var inactive_set = keys(message.guild.roles.cache.get("408675904110198795").members);
    var mod_set = keys(message.guild.roles.cache.get("408106863117336577").members);
    var retired_set = keys(message.guild.roles.cache.get("676854481933172771").members);
    var dev_set = keys(message.guild.roles.cache.get("637279237841354754").members);
    var admin_set = keys(message.guild.roles.cache.get("408106459910504458").members);
    var merged = union(admin_set, union(mod_set, union(dev_set, union(mentor_set, union(inactive_set, retired_set)))));

	for(var key of merged){
		var member = message.guild.member(key);
		var name;
		if(member.nickname == undefined || member.nickname == null){
			name =  member.user.username;
		}else{
			var name = member.nickname;
		}

 		if(name.includes("[")){
 			name = name.split("[")[0];
 		}
 		if(name.includes("|")){
 			name = name.split("|")[0];
 		}
		if(name.includes("(")){
			name = name.split("(")[0];
		}
		
        text = text + name + " (";
		var first = true; 

		if(member.roles.cache.has("408106459910504458")){
			text = text + "admin";
			first = false;
		}
	if(member.roles.cache.has("408106863117336577")){
        if(!first){
        	text = text + "/";
        }
        text = text + "mod";
        first = false;
    }
     if(member.roles.cache.has("637279237841354754")){
        if(!first){
        	text = text + "/";
        }
        text = text + "developer";
        first = false;
    }
     if(member.roles.cache.has("408107637100511243")){
        if(!first){
        	text = text + "/";
        }
        text = text + "active mentor";
        first = false;
    }
     if(member.roles.cache.has("408675904110198795")){
        if(!first){
        	text = text + "/";
        }
        text = text + "inactive mentor";
        first = false;
    }
     if(member.roles.cache.has("676854481933172771")){
        if(!first){
        	text = text + "/";
        }
        text = text + "retired mentor";
        first = false;
    }
     if(member.roles.cache.has("520749698424766475")){
        if(!first){
        	text = text + "/";
        }
        text = text + "cox mentor";
        first = false;
    }
     if(member.roles.cache.has("520750027405262858")){
        if(!first){
        	text = text + "/";
        }
        text = text + "tob mentor";
        first = false;
    }		

		text = text + ")\n";
	}

    fs.writeFile('Roster.txt', text , (err) => { 
    	if (err) { throw err }  
	})
	
	message.channel.send("", { files: ["./Roster.txt"] }); 

  	function keys(arg){
    	var k = new Set();
    	for(var [key, value] of arg){
      		k.add(key)
    	}
    	return k;
  	}

    function union(setA, setB) {
        let _union = new Set(setA)
        for (let elem of setB) {
            _union.add(elem)
        }
        return _union
    }



}

exports.conf = {
 enabled: true,
 guildOnly: false,
 aliases: [],
 permLevel: permLevel.TRIALMOD
};

exports.help = {
 name: 'roster',
 description: 'generate current staff roster',
 usage: 'roster'
};
