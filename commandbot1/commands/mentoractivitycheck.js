exports.run = async function(client, message){
  await message.channel.send("Getting mentor activity log from the last month");
  await message.guild.members.fetch();
  let connection = client.connection;

  let mentorRole = "408107637100511243";
  let mentors = keys(message.guild.roles.cache.get(mentorRole).members);

  let inactive = "408675904110198795";
  let inactives = keys(message.guild.roles.cache.get(inactive).members);

  let coxMentor = "520749698424766475";
  let tobMentor = "520750027405262858";



  let fromSql = new Map()

  let inactiveQuotaFail = new Array();

  let coxQuotaFail = new Array();
  let tobQuotaFail = new Array();
  let totalQuotaFail = new Array();
  let quotaReached = new Array();

  //get all users of last month
  let sql_command = "SELECT user_id, message, count(*) AS amount FROM WDR.MENTOR_RAID_LOGS WHERE YEAR(msg_time ) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(msg_time ) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) GROUP BY user_id, message"

  await connection.query(sql_command, async function(error, result, fields){
    if(error) { throw error }
    if(result != undefined && result != null){
      let data = result;
      if(data.length == 0){
        message.channel.send("Noone met quota.");
        return;
      }
      for(let i = 0; i < data.length; i++){
        if(!fromSql.has(data[i].user_id)){
          fromSql.set(data[i].user_id, {[data[i].message]: data[i].amount});
          continue;
        }
        let mapData = fromSql.get(data[i].user_id);
        mapData[data[i].message] = data[i].amount;
        fromSql.set(data[i].user_id, mapData);
      }


      mentors.forEach(el => {
        el = message.guild.members.cache.get(el);
        if(el.roles.cache.has(coxMentor) && el.roles.cache.has(tobMentor)){

          let userData = fromSql.get(el.id);
          if(userData === undefined){
            coxQuotaFail.push("<@!"+el.id+">");
            tobQuotaFail.push("<@!" + el.id + ">");
            totalQuotaFail.push("<@!"+el.id+">");
          }
          else{
            if(sum(userData) < 4){
              coxQuotaFail.push("<@!"+el.id+">");
              tobQuotaFail.push("<@!" + el.id + ">");
              totalQuotaFail.push("<@!"+el.id+">");
            }
            else{
              if(userData["cox"] === undefined)
                userData["cox"] = 0;
              if(userData["adv"] === undefined)
                userData["adv"] = 0;
              if(userData["cm"] === undefined)
                userData["cm"] = 0;
              if(userData["cox trial"] === undefined)
                userData["cox trial"] = 0;
              if(userData["tob"] === undefined)
                userData["tob"] = 0;
              if(userData["tob trial"] === undefined)
                userData["tob trial"] = 0;

              let coxOther;
              coxOther = userData["adv"]+userData["cm"]+userData["cox trial"];
              if(coxOther > 2)
                coxOther = 2;

              if(userData["cox"]+coxOther >= 4){
                if(userData["tob"]+userData["tob trial"] < 2){
                  tobQuotaFail.push("<@!"+el.id+">");
                }
              }
              else if(userData["tob"]+userData["tob trial"] >= 4){
                if(userData["cox"]+coxOther < 2){
                  coxQuotaFail.push("<@!"+el.id+">");
                }
              }
              else{
                coxQuotaFail.push("<@!"+el.id+">");
                tobQuotaFail.push("<@!" + el.id + ">");
                totalQuotaFail.push("<@!"+el.id+">");
              }
            }
          }


        }
        else if(el.roles.cache.has(coxMentor)){
          let userData = fromSql.get(el.id);
          if(userData === undefined){
            coxQuotaFail.push("<@!"+el.id+">");
            totalQuotaFail.push("<@!"+el.id+">");
          }
          else if(userData["cox"] < 2){
            coxQuotaFail.push("<@!"+el.id+">");
            totalQuotaFail.push("<@!"+el.id+">");
          }
        }
        else if(el.roles.cache.has(tobMentor)){

          let userData = fromSql.get(el.id);

          if(userData === undefined){
            tobQuotaFail.push("<@!"+el.id+">");
            totalQuotaFail.push("<@!"+el.id+">");
          }
          else {
            if (userData["tob trial"] === undefined) {
              userData["tob trial"] = 0;
            }
            if (userData["tob"] === undefined) {
              userData["tob"] = 0;
            }
            if (userData["tob"] + userData["tob trial"] < 4) {
              tobQuotaFail.push("<@!" + el.id + ">");
              totalQuotaFail.push("<@!" + el.id + ">");
            }
          }
        }

        let userData = fromSql.get(el.id);
        if(userData === undefined){
          userData = {"cox": 0}
        }
        if(sum(userData) < 4){
          if(totalQuotaFail.indexOf("<@!"+el.id+">") === -1)
            totalQuotaFail.push("<@!"+el.id+">");
        }


      });

      inactives.forEach(el => {
        el = message.guild.members.cache.get(el);
        let userData = fromSql.get(el.id);
        if(userData === undefined){
          userData = {"cox": 0}
        }
        if(sum(userData) < 4){
          inactiveQuotaFail.push("<@!"+el.id+">");
        }
      });

      fromSql.forEach( (el, key) =>{
        let user = message.guild.members.resolve(key.toString());
        if(user == null){
          return;
        }
        if(user.roles.cache.has(mentorRole))
          return;
        if(el["cox"] >= 4){
          quotaReached.push("<@!"+key+"> (CoX)");
        }
        if(el["tob"]+el["tob trial"] >= 4){
          quotaReached.push("<@!"+key+"> (ToB)");
        }
        if(el["cox"]+el["tob"] >= 4 && el["cox"] >= 2 && el["tob"] >= 2){
          quotaReached.push("<@!"+key+"> (Both)")
        }

      })

      let msg = "**Mentor activity check:**\n";

      msg += "**Tob quota fails:**\n";
      msg += tobQuotaFail.join("\n");
      msg += "\n\n**Cox quota fails:**\n";
      msg += coxQuotaFail.join("\n");
      msg += "\n\n**Quota fails:**\n";
      msg += totalQuotaFail.join("\n");
      msg += "\n\n**Quota fails and inactive:**\n"
      msg += inactiveQuotaFail.join("\n");
      msg += "\n\n**Quota reached but not active mentor:**\n"
      msg += quotaReached.join("\n");
      msg += "\n\n*This command is still in it's early stages, please double check if I missed anyone.*";

      message.channel.send(msg);

    }


  });

  function sum(obj) {
    return Object.keys(obj).reduce((sum,key)=>sum+parseFloat(obj[key]||0),0);
  }
  function keys(arg){
    var k = new Set();
    for(var [key, value] of arg){
      k.add(key)
    }
    return k;
  }
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["mac"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "mentor-activity-check",
  description: "Gets all active mentors who didn't meet quota",
  usage: "mentor-activity-check"
};
