const Discord = require("discord.js");
exports.run = async function(client, message, args){
  await message.guild.members.fetch();
  const updatePlayerCount = require ("../util/updatePlayerCount.js");

  updatePlayerCount(client, message);


  var command = args.join(" ")
  if (command === "misc"){
    let total = message.guild.memberCount;
    let nitro = message.guild.roles.cache.get("585528815229599775").members.size;
    let ffa = message.guild.roles.cache.get("408222042492764160").members.size;
    let ironman = message.guild.roles.cache.get("408222039569334282").members.size;
    let mass = message.guild.roles.cache.get("408221081057492993").members.size;
    let advmass = message.guild.roles.cache.get("442125897324036106").members.size;
    let learnermass = message.guild.roles.cache.get("551110999822958598").members.size;
    let event = message.guild.roles.cache.get("408222125573537794").members.size;
    let cmintro = message.guild.roles.cache.get("613871676572041236").members.size;
    let europe = message.guild.roles.cache.get("408248019339837441").members.size;
    let useast = message.guild.roles.cache.get("408248033034371082").members.size;
    let uswest = message.guild.roles.cache.get("408248047974350848").members.size;
    let oeania = message.guild.roles.cache.get("408248062658609153").members.size;
    let weebs = message.guild.roles.cache.get("535282475207884803").members.size;
    
    let punished = message.guild.roles.cache.get("408221074833145863").members.size;
    let nonraider = message.guild.roles.cache.get("442590109599268864").members.size;
    let diary = message.guild.roles.cache.get("669399012201463808").members.size;

    let three = message.guild.roles.cache.get("408247451217166336").members.size;
    let four = message.guild.roles.cache.get("408247454207705088").members.size;
    
    
    const embed = new Discord.MessageEmbed()
    .setTitle(`${message.guild.name} Member Counts`)
    .setColor(3447003)
  //.setThumbnail(`${message.guild.iconURL}`)
    .addField("FFA", ffa, true)
    .addField("Mass", mass, true)
    .addField("Adv Mass", advmass, true)
    .addField("Lrn Mass", learnermass, true)
    .addField("event", event, true)
    .addField("cmintro", cmintro, true)
    .addField("europe", europe, true)
    .addField("useast", useast, true)
    .addField("uswest", uswest, true)
    .addField("oeania", oeania)
    .addField("weebs", weebs, true)
    .addField("Nonraider", nonraider, true)
    .addField("Iron", ironman, true)
    .addField("diary", diary)
    .addField("punished", punished, true)
    .addField("three", three, true)
    .addField("four", four,true)
    .addField("\u200B", "\u200B")
      .setTimestamp();
  //message.guild.roles.cache.get('132123');
  //but if you want id with bot, just find it by name, and then add a .id and log it / send i
  message.channel.send({embed});
    
    return;
    

  }
  else if(args.join(" ").toLowerCase().includes("tier")) {
    let argsString = args.join(" ").toLowerCase();
    let start = argsString.indexOf("tier");
    let space = argsString.indexOf(" ", start);
    let tierIndex = ++space;
    if (tierIndex <= argsString.length) {
      let tierNumber = argsString[tierIndex];
      tierNumber = Number(tierNumber);
      if (tierNumber < 6 && tierNumber >= 0) {
        let tierMap = new Map();
        tierMap.set(0, ["408191409892360192", "694326623981535382"]);
        tierMap.set(1, ["408191464766439434", "520748851691388929"]);
        tierMap.set(2, ["408191506550358017", "520748989033873408"]);
        tierMap.set(3, ["408191842258124800", "591269156247044096"]);
        tierMap.set(4, ["408191873925251073", "520749089692975109"]);
        tierMap.set(5, ["408191912462516224", "591269143815127070"]);

        let tierId = tierMap.get(tierNumber)
        let coxRole = message.guild.roles.cache.get(tierId[0]).members.size;
        let tobRole = message.guild.roles.cache.get(tierId[1]).members.size;

        const embed = new Discord.MessageEmbed()
          .setTitle(`${message.guild.name} Member Counts`)
          .setColor(3447003)
          .addField("CoX tier " + tierNumber, coxRole, true)
          .addField("ToB tier " + tierNumber, tobRole, true)

        message.channel.send({embed});


        return;

      }
    }
  }
  else if(args.join(" ")  === "staff"){
 


  let mentor = message.guild.roles.cache.get("408107637100511243").members.size;
  let inactive = message.guild.roles.cache.get("408675904110198795").members.size;
  let mod = message.guild.roles.cache.get("408106863117336577").members.size;
  let retired = message.guild.roles.cache.get("676854481933172771").members.size;
  var active_mentors = message.guild.roles.cache.get("408107637100511243").members;
  let exmod = message.guild.roles.cache.get("826667898184925206").members.size;
  
  var mentor_set = keys(message.guild.roles.cache.get("408107637100511243").members);
  var inactive_set = keys(message.guild.roles.cache.get("408675904110198795").members);
  var mod_set = keys(message.guild.roles.cache.get("408106863117336577").members);
  var retired_set = keys(message.guild.roles.cache.get("676854481933172771").members);
  var dev_set = keys(message.guild.roles.cache.get("637279237841354754").members);
  var admin_set = keys(message.guild.roles.cache.get("408106459910504458").members);
  let exmod_set = keys(message.guild.roles.cache.get("826667898184925206").members);

  var merged = union(mentor_set, union(inactive_set, union(mod_set, union(retired_set, union(dev_set, union(admin_set, exmod_set))))));
  var staff_total = merged.size;
  
  var active_tob = 0;
  var active_cox = 0;
 
  for( var [key, value] of active_mentors){
    if(value.roles.cache.has("520749698424766475")){
      active_cox++;
    }
    if(value.roles.cache.has("520750027405262858")){
      active_tob++;
    }
  }

    const embed = new Discord.MessageEmbed()
    .setTitle(`${message.guild.name} Member Counts`)
    .setColor(3447003)
 
    .addField("Mod", mod, true)
    .addField("Mentor", mentor, true)
    .addField("Inactive", inactive, true)
    .addField("Retired", retired, true)
    .addField("Active COX mentor", active_cox, true)
    .addField("Active TOB mentor", active_tob, true)
    .addField("Ex mod", exmod, true)
    .addField("Staff Total", staff_total, true)
    .setTimestamp();
  //message.guild.roles.cache.get('132123');
  //but if you want id with bot, just find it by name, and then add a .id and log it / send i
  message.channel.send({embed});
    

    return;
}
 



  if(args.length > 0){
      var role_name = args.join(" ");
	  let role =  message.guild.roles.cache.find(role => role.name.toUpperCase() === role_name.toUpperCase()); 
      let role_count = message.guild.roles.cache.get(role.id).members.size;
      const embed = new Discord.MessageEmbed()
        .setTitle(`${message.guild.name} Member Counts`)
        .setColor(3447003)
        .addField(role.name, role_count, true)
        .setTimestamp();
      message.channel.send({embed});
      return;
  }

  let total = message.guild.memberCount;
  let nitro = message.guild.roles.cache.get("585528815229599775").members.size;
  let cox_tier5 = message.guild.roles.cache.get("408191912462516224").members.size;
  let cox_tier4 = message.guild.roles.cache.get("408191873925251073").members.size;
  let cox_tier3 = message.guild.roles.cache.get("408191842258124800").members.size;
  let cox_tier2 = message.guild.roles.cache.get("408191506550358017").members.size;
  let cox_tier1 = message.guild.roles.cache.get("408191464766439434").members.size;
  let cox_tier0 = message.guild.roles.cache.get("408191409892360192").members.size;
  
  let tob_tier0 = message.guild.roles.cache.get("694326623981535382").members.size;
  let tob_tier1 = message.guild.roles.cache.get("520748851691388929").members.size;
  let tob_tier2 = message.guild.roles.cache.get("520748989033873408").members.size;
  let tob_tier3 = message.guild.roles.cache.get("591269156247044096").members.size;
  let tob_tier4 = message.guild.roles.cache.get("520749089692975109").members.size;
  let tob_tier5 = message.guild.roles.cache.get("591269143815127070").members.size;

  let toblearner = message.guild.roles.cache.get("556557558761127936").members.size;
    let coxlearner = message.guild.roles.cache.get("644528392418623488").members.size;

  const embed = new Discord.MessageEmbed()
    .setTitle(`${message.guild.name} Member Counts`)
    .setColor(3447003)
  //.setThumbnail(`${message.guild.iconURL}`)
    .addField("Total Members", total, true)
    .addField("Nitro Boosters", nitro, true)
    .addField("\u200B", "\u200B") //addblankfield replacement
    .addField("Tier 5 (CoX)", cox_tier5, true)
    .addField("Tier 4 (CoX)", cox_tier4, true)
    .addField("Tier 3 (CoX)", cox_tier3, true)
    .addField("Tier 2 (CoX)", cox_tier2, true)
    .addField("Tier 1 (CoX)", cox_tier1, true)
    .addField("Tier 0 (CoX)", cox_tier0, true)
    .addField("coxlearner", coxlearner, true)
    .addField("No CoX Tier",total-cox_tier5-cox_tier4-cox_tier3-cox_tier2-cox_tier1-cox_tier0, true)
    .addField("\u200B", "\u200B")
    .addField("Tier 5 (ToB)", tob_tier5, true)
    .addField("Tier 4 (ToB)", tob_tier4, true)
    .addField("Tier 3 (ToB)", tob_tier3, true)
    .addField("Tier 2 (ToB)", tob_tier2, true)
    .addField("Tier 1 (ToB)", tob_tier1, true)
    .addField("Tier 0 (ToB)", tob_tier0, true)
    .addField("toblearner", toblearner, true)
    .addField("No ToB Tier",total-tob_tier5-tob_tier4-tob_tier3-tob_tier2-tob_tier1-tob_tier0,true)
    .addField("\u200B", "\u200B")
    .setTimestamp();
  //message.guild.roles.cache.get('132123');
  //but if you want id with bot, just find it by name, and then add a .id and log it / send i
  message.channel.send({embed});
  
  function keys(arg){
    var k = new Set();
    for(var [key, value] of arg){
      k.add(key)
    }
    return k;
  }
 
  function union(setA, setB) {
    let _union = new Set(setA)
    for (let elem of setB) {
      _union.add(elem)
    }
    return _union
  }

};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['uc'],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "usercount",
  description: "Server usercount",
  usage: "usercount"
};
