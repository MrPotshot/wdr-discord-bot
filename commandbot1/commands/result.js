exports.run = async function(client, message, args){
  const connection = client.connection;
  let admin = "408106459910504458"; //admin bypass

  let sql_command = "SELECT message_id, channelId FROM WDR.voteMessages WHERE message_id = ? AND open = true";
  let msgId = args[0];

  await connection.query(sql_command, [msgId], async function(error, result, fields) {
    if (error) {
      throw error
    }
    if (result.length === 0) {
      message.channel.send("No open vote by that id found");
      return;
    }
    let channel = message.guild.channels.resolve(result[0]['channelId']);

    let messageObject = await channel.messages.fetch(result[0]['message_id']);
    if(messageObject.member.id !== message.member.id && !message.member.roles.cache.has(admin)){
      message.channel.send("You must be the creator of this vote to use this command")
      return;
    }

    let sql_command_votes = "SELECT name, vote FROM WDR.voters_votes vv JOIN WDR.voters v ON vv.user_id = v.user_id WHERE message_id = ?"
    await connection.query(sql_command_votes, [messageObject.id], async function(error, result_votes, fields) {
      if (error) {
        throw error
      }

      if (result_votes.length === 0) {
        message.author.send("Noone has voted for this vote.")
        let sql_vote_close = "UPDATE WDR.voteMessages SET open = 0 WHERE message_id = ?;";
        await connection.query(sql_vote_close, [msgId]);
        return;
      }

      let arr = [];

      for (let votes of result_votes) {
        if (arr[votes.vote] === null || arr[votes.vote] === undefined)
          arr[votes.vote] = [];
        arr[votes.vote].push(votes['name']);

      }
      let msg = 'Vote closed for '+msgId+'\n\n';
      for (let el in arr) {
        msg += `${el} - ${arr[el].length} (${arr[el].join(", ")})`
        msg += '\n';
      }
      await messageObject.reply(msg);
      let sql_vote_close = "UPDATE WDR.voteMessages SET open = 0 WHERE message_id = ?;";
      await connection.query(sql_vote_close, [msgId]);
    });

  });

}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['close'],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'result',
  description: 'closes anonymous vote',
  usage: 'anonvote | votes | messageid'
};
