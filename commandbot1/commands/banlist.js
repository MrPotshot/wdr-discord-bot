exports.run = async function(client, message, args){
    const {google} = require("googleapis"); //npm install googleapis@39 --save
    const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
    const Discord = require("discord.js");
    const banlistUpdate = require ("../util/banlistupdate.js");
		const toJagexName = require ("../util/toJagexName.js");
		const fetch = require("node-fetch");
		const RwEndPoint = "https://raw.githubusercontent.com/while-loop/runelite-plugins/runewatch/data/mixedlist.json";


    var Banlist_Column_Headers = [];

	const google_client = new google.auth.JWT(
	    google_sheet_keys.client_email,
	    null,
	    google_sheet_keys.private_key,
	    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
    );

    google_client.authorize(function(error, tokens){
	    if(error){
		    console.log(error);
		    return;
	    }
    });

	var rsns = args.join(" ").trim();

	if(rsns.length == 0){
		message.channel.send("Invalid Usage");
		return;
	}
	
    if(message.content.startsWith("!banlist")){
        message.channel.send("!rw " + rsns);
    }
	banlistUpdate(client);

	const google_sheet = google.sheets({version:"v4", auth:google_client})
	const opt= {
		spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
		range: "Data"
	}

	var data = await google_sheet.spreadsheets.values.get(opt);
	var banlist = data.data.values;


	Banlist_Column_Headers = banlist[0];

	var names = new Set();

	for(var i = 1; i < banlist.length; i++){
		if(banlist[i][8] == undefined){
			continue;
		}
	  if(banlist[i][8].startsWith("Repaid")) {
			continue;
		}
		if(banlist[i][0] != undefined && banlist[i][0] != "" && banlist[i][0] != null){
			names.add(banlist[i][0])
		}
		if(banlist[i][2] != undefined && banlist[i][2] != ""){
			var alt_names = banlist[i][2].split(",").map(function (x){ return x.trim()});
			for(var j = 0; j < alt_names.length; j++){
				if(alt_names[j] != undefined && alt_names[j] != "" && alt_names[j] != null){
				    names.add(alt_names[j]);
				}
			}
		}
	}

	let response = await fetch(RwEndPoint);
	if(response.status === 200){
		let json = await response.text();
		let RwBanlistArray = JSON.parse(json);
		let rwEntries = RwBanlistArray.filter(entries => entries.source === "RW");
		let rsnsArr = rsns.split(",");
		let tempNames = rsnsArr.map(e =>{ return e.toUpperCase().trim()})
		let RelevantRwEntries = rwEntries.filter(entries => tempNames.includes(entries.accused_rsn.toUpperCase()))
		for(let entry of RelevantRwEntries){
			let info = `__**Accused rsn**__: ${entry.accused_rsn}`;
			info += `\n__**Reason**__: ${entry.reason}`;
			info += `\n__**Evidence rating**__: ${entry.evidence_rating}/5`;
			info += `\n__**Date**__: ${entry.published_date}`

			let embed = new Discord.MessageEmbed().setColor(12949258);
			embed.setTitle("__**RW Case**__");
			embed.setDescription(info);
			embed.setTimestamp(new Date());
			embed.setFooter("WDR Bot");
			message.channel.send(embed);
		}

	}



	names.delete(undefined);
	let rsnsArr = rsns.split(",");
	for(var rsn of rsnsArr) {
		rsn = toJagexName(rsn);
		var edit_distance_arr = [];
		var match_found = false;
		var match_row = null;
		let blankRsn = false;
		if(rsn !== "") {
			for (var i = 0; i < banlist.length; i++) {
				if (banlist[i][8] == undefined) {
					continue;
				}
				if (banlist[i][8].startsWith("Repaid")) {
					continue;
				}
				if (banlist[i][0] != undefined && banlist[i][0].trim().toUpperCase() == rsn.toUpperCase() || (banlist[i][2] != undefined && banlist[i][2].split(",").map(function (x) {
					return x.trim().toUpperCase()
				}).includes(rsn.toUpperCase()))) {
					match_row = banlist[i];
					match_found = true;
				}
			}
		}
		else{
			blankRsn = true;
		}
		if (match_found) {
			var info = "";

			for (var i = 0; i < Banlist_Column_Headers.length; i++) {
				if (match_row[i] != undefined && match_row[i] != "" && i < 6) {
					info += "__**" + Banlist_Column_Headers[i] + "**__: " + match_row[i] + "\n";
				}
			}

			var embed = new Discord.MessageEmbed().setColor(293680)
			embed.setTitle("__**WDR Banlist**__");
			embed.setDescription(info);
			embed.setTimestamp(new Date());
			embed.setFooter("WDR Bot");
			message.channel.send(embed);

		} else {
			if(!blankRsn) {
				message.channel.send("\"" + rsn + "\" is not on the WDR banlist");
				for (var n of names) {
					if ((rsn.length <= 3 && edit_distance(n.toUpperCase(), rsn.toUpperCase()) == 1) || (rsn.length > 3 && edit_distance(n.toUpperCase(), rsn.toUpperCase()) <= 2)) {
						edit_distance_arr.push(n)
					}
				}
				var close_match_string = "close matches:\n```";
				for (var i = 0; i < edit_distance_arr.length; i++) {
					close_match_string += edit_distance_arr[i] + "\n";
				}
				close_match_string += "```";
			}
			if (edit_distance_arr.length > 0) {
				if (close_match_string.length >= 2000) {
					message.channel.send("too many close matches to display")
				}
				message.channel.send(close_match_string);
			}
		}
	}

	function edit_distance(a, b) {
	  	const distanceMatrix = Array(b.length + 1).fill(null).map(() => Array(a.length + 1).fill(null));

  		for (let i = 0; i <= a.length; i += 1) {
    		distanceMatrix[0][i] = i;
  		}	

  		for (let j = 0; j <= b.length; j += 1) {
    		distanceMatrix[j][0] = j;
  		}

  		for (let j = 1; j <= b.length; j += 1) {
    		for (let i = 1; i <= a.length; i += 1) {
      			const indicator = a[i - 1] === b[j - 1] ? 0 : 1;
      			distanceMatrix[j][i] = Math.min(
        			distanceMatrix[j][i - 1] + 1, // deletion
        			distanceMatrix[j - 1][i] + 1, // insertion
        			distanceMatrix[j - 1][i - 1] + indicator, // substitution
      			);
    		}
  		}
  		return distanceMatrix[b.length][a.length];
	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["rsj","rw"],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'banlist',
  description: 'looks up rsns on the wdr banlist',
  usage: 'banlist rsn1, rsn2'
};
