
module.exports = async (reaction, user) => {
  if(reaction.message.partial) await reaction.message.fetch();
  if (reaction.partial) await reaction.fetch();
  if(user.bot)
    return;

  const client = user.client;
  const connection = client.connection;
   let modlog = client.channels.cache.get('417638357959573505');


  //lfr-staff reactions
  if(reaction.message.id == "803779323790098493"){
      let role = "";
      if(reaction._emoji.id == "651156619824070687"){ //cox
        role = "cox";
      }else if(reaction._emoji.id == "710582107994325132"){ //cm
        role = "cm";
      }else if(reaction._emoji.id == "702205226681237505"){ //tob
        role = "tob";
      }else if(reaction._emoji.id == "851730472747663391") {//tob hm
        role = "hm";
      }else if(reaction._emoji.id == "675667021157105694") { //Nightmare
        role = "nm"
      }else if(reaction._emoji.id == "634101902745010197"){ //CoX helper
        role = "cox-helper"
      }else if(reaction._emoji.id == "634101949700112390"){ //ToB helper
        role = "tob-helper"
      }else if(reaction._emoji.id == "353679633239113731"){ //BA
        role = "ba"
      }
    let sql_command = "INSERT INTO WDR.LFR_STAFF (discord_id, activity) VALUES (\'" + user.id + "\',\'" + role + "\')";
    connection.query(sql_command, async function(error, result, fields){
      if(error) { throw error }
      let userToMsg = await reaction.message.guild.members.fetch(user.id);
      userToMsg.send("lfr-staff " + role + " added")
    });
    return;
  }

  if(reaction.message.channel.id === "624718272926842900") { //staff-memes
    if(reaction.message.createdTimestamp < Date.now()-14){
      if(reaction._emoji.name === "❌"){
        if(reaction.count >= 10){
          reaction.message.delete();
        }
      }
    }
  }
  //modmail reactions
  if(reaction.message.channel.id === modlog.id){
    if(reaction.message.reactions.length > 1){
      return;
    }
    let uid = user.id;
    if(reaction._emoji.name === "❌")
      uid = "-1"
    let sql_command_modmail = "UPDATE WDR.MODMAIL SET user_reacted = '"+uid+"', modmail_reacted_time = '"+ new Date().toISOString().slice(0, 19).replace('T', ' ') +"'  WHERE message_id = '"+reaction.message.id+"' AND user_reacted IS NULL;";
    connection.query(sql_command_modmail, function(error, result, fields){
      if(error) { throw error }
      if(result.affectedRows > 0){
        let sql_command_modmail_2;
        if(reaction._emoji.name === "❌"){
          sql_command_modmail_2 = "SELECT mm.user_modmailed FROM WDR.MODMAIL mm WHERE message_id = '"+reaction.message.id+"'";
          connection.query(sql_command_modmail_2, async function(error, result, fields) {
            if (error) {
              throw error
            }
            try {
              let userid = result[0].user_modmailed
              let user = await reaction.client.users.fetch(userid)
              await user.send("Your latest modmail lacks some essential information. Please resubmit it with the following:\n" +
                "<:cox:651156619824070687> A description of what happened.\n" +
                "<:cox:651156619824070687> Names and discord accounts of everybody involved.\n" +
                "<:cox:651156619824070687> Links to non-embedded screenshots or other evidence.")
            } catch (e) {
              if(e.name == "DiscordAPIError"){
                reaction.message.channel.send("error `"+ e.message + "` occured on the following modmail: \nhttps://discordapp.com/channels/408082929940430858/417638357959573505/"+reaction.message.id);
              }
            }
          });
        }
        else{
          sql_command_modmail_2 = "SELECT mm.user_modmailed FROM WDR.MODMAIL mm WHERE message_id = '"+reaction.message.id+"'";
          connection.query(sql_command_modmail_2, async function(error, result, fields) {
            if (error) {
              throw error
            }
            try {
              let userid = result[0].user_modmailed
              let user = await reaction.client.users.fetch(userid)
              await user.send("Your modmail has been addressed. If we need anything more from you a Mod will contact you.");
            } catch (e) {
              console.log(e);
            }
          });
        }
      }
    });
    return;
  }

  //verzik-verification reactions
  if (reaction.message.channel.id == "556879235059548161"){ //only mentor+ has perms in that channel anyway, bad practice but I accidentally fucked up the entire bot


      let verzik = await reaction.message.guild.roles.fetch("556557558761127936");
      let user = await reaction.message.guild.members.fetch(reaction.message.author);
      if (!user) {
        console.log("something went wrong with fetching the user")
        return;
      }
      await user.roles.add(verzik).catch(console.error);
      reaction.message.delete();


  }

  if (reaction.message.channel.id == "744028112454549574"){
      let coxadv = await reaction.message.guild.roles.fetch("744026872467161108");
      let user = await reaction.message.guild.members.fetch(reaction.message.author);
      if (!user) {
        console.log("something went wrong with fetching the user")
        return;
      }
      await user.roles.add(coxadv).catch(console.error);
      reaction.message.delete();
  }
  //Anonymous votes stuff.
  let channels = ["567871852572049429", "471165593605898250", "555830683935965184", "851974390895083530"] //#updates, #crc, #trc, more in future

  if(channels.includes(reaction.message.channel.id)){
    let sql_command = "SELECT * FROM WDR.voteMessages WHERE message_id = "+reaction.message.id + " AND open = 1 LIMIT 1;";
    connection.query(sql_command, async function(error, result, fields) {
      if (error) {
        throw error
      }
      try {
        if(result.length >= 1){
          await reaction.users.remove(user);
          if(reaction.emoji.name === '🗑️'){
            let sql_remove_vote = 'DELETE FROM WDR.voters_votes WHERE voters_votes.message_id = ? AND voters_votes.user_id = ?'
            await connection.query(sql_remove_vote, [reaction.message.id, user.id]);
            await user.send("Your vote for message https://discord.com/channels/"+reaction.message.guild.id+"/"+reaction.message.channel.id+"/"+reaction.message.id+" has been removed.");
            return;
          }
          let displayName = (await reaction.message.guild.members.fetch(user.id)).displayName;
          let sql_command_add_mod = "INSERT INTO WDR.voters VALUES (?, ?) ON DUPLICATE KEY UPDATE name = ?;"
          let sql_command_vote = " INSERT INTO WDR.voters_votes VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE vote = ?;"
          await connection.query(sql_command_add_mod, [user.id, displayName, displayName]);
          await connection.query(sql_command_vote, [null, reaction.message.id, user.id, reaction.emoji.toString(), reaction.emoji.toString()]);
          await user.send("You voted "+reaction.emoji.toString()+" for message https://discord.com/channels/"+reaction.message.guild.id+"/"+reaction.message.channel.id+"/"+reaction.message.id);


          let sql_command_votes = "SELECT user_id, vote FROM WDR.voters_votes vv JOIN WDR.voteMessages v ON vv.message_id = v.message_id WHERE vv.message_id = ?"
          await connection.query(sql_command_votes, [reaction.message.id], async function(error, result_votes, fields) {
            if (error) {
              throw error
            }

            if (result_votes.length === 0) {
              return;
            }

            let counter = 0;
            let counterAll = 0;
            for (let votes of result_votes) {
              counterAll++;
              if(votes.vote === reaction.emoji.toString())
                counter++;
            }
            if(counterAll === 15){
              reaction.message.author.send("There have been a total of 15 votes cast.");
            }
            if(counter === 10)
              reaction.message.author.send(reaction.emoji.toString() +" has reached 10 votes.\nMake sure to double check if it's still the case since someone could have unreacted");
          });

        }
      }
      catch(error){
        console.log(error);
      }
    });

  }




};
