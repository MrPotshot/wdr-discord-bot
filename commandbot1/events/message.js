const settings = require("../settings.json");
const namestuff = require('../util/namestuff.js');
const invitefilter = require('../util/invitefilter.js');
const dumbquote = require('../util/dumbQuote.js');
const mentorRaidLogUpdate = require('../util/mentorRaidLogUpdate');
const {google} = require("googleapis"); //npm install googleapis@39 --save
const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
const Discord = require("discord.js");

  const google_client = new google.auth.JWT(
      google_sheet_keys.client_email,
      null,
      google_sheet_keys.private_key,
      ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
    );

    google_client.authorize(function(error, tokens){
      if(error){
        console.log(error);
        return;
      }
    });

module.exports = async message => {


  if(message.partial) await message.fetch();
  //all channels but cox advanced, cox cm, and tob-max-eff. Same order as channels are in the server, aus is included
  let lfrChannels = ["408265938610159616", "436565803811209216", "477140139894898688", "409083517650075648", "553467453955506176",
  "694328044474859541", "521410027597070366", "521410086128582692", "521410163425542165", "521410269390438404", "746535603700629584"];

  let client = message.client;
  const connection = client.connection;
  var rsnLogsId = "439098363632287749"; //RSN-logs
  var rolecommands = "521409656489377813";
  if(message.channel.id === rsnLogsId) { //RSN logs checks for @here
    if (message.author.id === "228019028755611648") { //Twisty bot
      if (message.embeds[0] !== undefined) {
        if (!message.embeds[0].fields[0].embed.description.startsWith("**This post was returned because the name") && message.embeds[0].fields[0].name === "Details:") {
          message.channel.send("@here");
          return;
        }
      }
    }
    if(message.author.id === message.client.user.id){
      if (message.embeds[0] !== undefined) {
        if (message.embeds[0].title === "__**WDR Banlist**__" || message.embeds[0].title === "__**RW Case**__") {
          message.channel.send("@here");
          return;
        }
      }
    }
  }
  if (message.author.bot && message.channel.id !== rsnLogsId) //if not in #rsn-logs, ignore bots
    return;
  if (!message.guild) { //Ignore dms
    message.author.send("Please do not message the bot, use the server to run commands.");
    return;
  }
  if(lfrChannels.includes(message.channel.id)){
    if( message.content.toLowerCase() == "lfr" || message.content.toLowerCase() == "lft" ) {
      message.author.send("Your post has been deleted automatically. Please refrain from posting just `lfr`/`lft`, and instead try to actively look for people (`+2 need mage and range/+2 bring zgs or waters`).");
      message.delete();
      return;
    }
  }
  if(message.channel.id == "458973214551572480"){ //Fellor's Pog Screenshots server
      dumbquote(message, client)
  }
  if(message.channel.id == rolecommands){ //Ignore role commands, because people like to use !kc
    return;
  }
  if (message.content.startsWith(settings.prefix) &&message.channel.id === "500193114506526720"){ //ignore messages in post-your-rsn-here that start with !
    message.member.send("Do not use !, follow the instructions.");
    message.delete();
    return;
  }
  if(message.channel.id === "707656261063147560"){ //Mentor raids logs stuff
    mentorRaidLogUpdate(message, client);
    return;
  }
  if(message.channel.id === "408188250264436737"){ //Server-suggestions
    await message.react(message.guild.emojis.cache.get('588443221986770993'));
    await message.react(message.guild.emojis.cache.get('837851125289451520'));
    return;
  }

  let perms = await client.elevation(message);
  //making client here so we can check for perms before invitefilter

  if(invitefilter(message) && perms < permLevel.TRIALMOD) { //do the invite filter if not tmod+
    message.member.send("It looks like you tried to send an unapproved invite link. If you think this was an error, please message a mod or admin. You can find our whitelist here: <https://docs.google.com/spreadsheets/d/1NO7WJg2Jb983O28mEAnNZTc5Fgnioy2ee3GKbHpU1nQ>");
    message.delete();
    return;
  }
  if (!message.content.startsWith(settings.prefix)){ //Check if the message starts with !
    if(message.channel.id === "500193114506526720"){//that is rsn name change
      namestuff(message, client); //this is all the of old rsn name change code
    }
    return;
  }


  let args = message.content.split(/\s+/g).slice(1);
  let command = message.content.split(/\s+/g)[0].slice(settings.prefix.length);
  command = command.toLowerCase();

  let cmd;
  if (client.commands.has(command)) {
    cmd = client.commands.get(command);
  } else if (client.aliases.has(command)) {
    cmd = client.commands.get(client.aliases.get(command));
  }
  if (cmd) { //standard command
    if (perms < cmd.conf.permLevel) return;
    if(perms < permLevel.TRIALMOD){//Traril mod+ bypass channel check
      const google_sheet = google.sheets({version:"v4", auth:google_client})
      const opt= {
        spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
        range: "Command_Perms"
      }
      var data = await google_sheet.spreadsheets.values.get(opt);
      var command_perms = data.data.values;
      for(var i = 1; i < command_perms.length; i++){
        if(cmd.help.name == command_perms[i][0]){
          var channels = command_perms[i][2].split(",").map(function(val){
            if(val.indexOf("#") != -1){
              return val.substring(0, val.indexOf("#") - 1).trim();
            }else{
              return val.trim();
            }
          })
          if(command_perms[i][1] == "TRUE"){//whitelist
            if(!channels.includes(message.channel.id)){
              var error_message = "You can only use this command in: ";
              for(var j = 0; j < channels.length; j++){
                error_message += " <#" + channels[j] + ">";
              }
              message.channel.send(error_message);
              let sql_command = `INSERT INTO WDR.COMMAND_USAGE(message_id, user_id, command_name, message, command_time, type, channel_id, Error) VALUES('${message.id}', '${message.author.id}', '${cmd.help.name}', ${connection.escape(message.content)}, NULL, 0, '${message.channel.id}', TRUE);`;
              await connection.query(sql_command, function(error, result, fields){
                if(error){ throw error }
              })
              return;
            }
          }else{//blacklist
            if(channels.includes(message.channel.id)){
              var error_message = "You cannot use this command in: ";
              for(var j = 0; j < channels.length; j++){
                error_message += " <#" + channels[j] + ">";
              }
              message.channel.send(error_message);
              let sql_command = `INSERT INTO WDR.COMMAND_USAGE(message_id, user_id, command_name, message, command_time, type, channel_id, Error) VALUES('${message.id}', '${message.author.id}', '${cmd.help.name}', ${connection.escape(message.content)}, NULL, 0, '${message.channel.id}', TRUE);`;
              await connection.query(sql_command, function(error, result, fields){
                if(error){ throw error }
              })
              return;
            }
          }
        }
      }
    }
    cmd.run(client, message, args, perms);
    let sql_command = `INSERT INTO WDR.COMMAND_USAGE(message_id, user_id, command_name, message, command_time, type, channel_id, Error) VALUES('${message.id}', '${message.author.id}', '${cmd.help.name}', ${connection.escape(message.content)}, NULL, 0, '${message.channel.id}', FALSE);`;
    await connection.query(sql_command, function(error, result, fields){
      if(error){ throw error }
    })


  }else{//message command from spreadsheet
    const google_sheet = google.sheets({version:"v4", auth:google_client})
    const opt= {
      spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
      range: "message"
    }
    var data = await google_sheet.spreadsheets.values.get(opt);
    var message_commands = data.data.values;
    for(var i = 0; i < message_commands.length; i++){
      if(message_commands[i][0].split(",").map(function(val){ return val.toLowerCase().trim()}).includes(command)){
        if(message_commands[i][5] !== undefined && !isNaN(message_commands[i][5])){
          if(perms < message_commands[i][5])
            return;
        }

        let msg = message_commands[i][1];

        if(message_commands[i][2] === "FALSE") {
          message.channel.send(msg);
          let sql_command = `INSERT INTO WDR.COMMAND_USAGE(message_id, user_id, command_name, message, command_time, type, channel_id, Error) VALUES('${message.id}', '${message.author.id}', '${message_commands[i][0].split(",")[0]}', ${connection.escape(message.content)}, NULL, 1, '${message.channel.id}', FALSE);`;
          await connection.query(sql_command, function(error, result, fields){
            if(error){ throw error }
          })
          return;
        }
        const embed = new Discord.MessageEmbed()
        let match = msg.match(/((https|http)?:\/\/.*\.(?:png|jpg|gif))/i);

        if(match && message_commands[i][4] === "FALSE"){
          embed.setImage(match[0]);
          msg = msg.replace(match[0], '')
        }

        embed.setDescription(msg)

        if(message_commands[i][3] !== undefined && message_commands[i][3].includes(',')) {
          let color = message_commands[i][3].split(",").map(Number)
          if (color.length === 3) {
            if (!color.some(el => el <= 255))

              console.log(color);
              embed.setColor(color);
          }
        }
        message.channel.send("", {embed: embed})
        let sql_command = `INSERT INTO WDR.COMMAND_USAGE(message_id, user_id, command_name, message, command_time, type, channel_id, Error) VALUES('${message.id}', '${message.author.id}', '${message_commands[i][0].split(",")[0]}', ${connection.escape(message.content)}, NULL, 1, '${message.channel.id}', FALSE);`;
        await connection.query(sql_command, function(error, result, fields){
          if(error){ throw error }
        })
        return;
      }
    }
  }
};
