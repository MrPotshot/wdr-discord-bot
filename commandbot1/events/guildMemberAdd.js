const Discord = require("discord.js");

module.exports = async member => {
  if(member.partial) await member.fetch();
  const client = member.client;
  const connection = client.connection;
  //console.log(member.id + " joined")
  var sql_command_1 = "SELECT * FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + member.id + "\'";
  connection.query(sql_command_1, function(error, result, fields){
    if(error) { throw error }
    if(result == undefined || result == null || result.length == 0){
      return;
    }
    var roles = JSON.parse(result[0].roles);
    for(var i = 0; i < roles.length; i++){
      if(roles[i] == "408082929940430858"){ //check if the role is @everyone
        continue;
      }
      var role = member.guild.roles.cache.get(roles[i]);

      if(role != undefined && role != null){
        member.roles.add(role)
      }
    }
    sql_command_2 = "DELETE FROM WDR.OLD_USER_ROLES WHERE discord_id=\'" + member.id + "\'";
    connection.query(sql_command_2, function(error, result, fields){
      if(error) { throw error }
    })
  });

  var welcomemsg = "Welcome to the discord server of **We Do Raids**!\nThis is a community of people whose aim is to teach others how to raid and help experienced people find others to raid with!\nBefore you begin posting, please refer to the #welcome channel for server information, etiquette and general help.\nThe Welcome channel will have information on how to gain access to the full server.\nWe have had some scammers come in so please keep an eye out for sketchy links.\nBy using this discord server and the bot you agree to the terms of service of the bot. You can read these by typing !tos  \n**READ THE WELCOME CHANNEL TO GET STARTED**";
  member.send(welcomemsg).catch(console.error);

  const channel = member.guild.channels.cache.find(channel => channel.name === "logs");
  if (!channel) return;

  let memberCreated = member.user.createdAt;
  let ping = member.toString();
  if(Date.now() - memberCreated <= 86400000){
    let timeSinceCreation = Date.now() - memberCreated;

    let humanTime = new Date(timeSinceCreation).toISOString().slice(11,19);
    ping += ` (new account created ${humanTime} ago)`;
  }

  const embed = new Discord.MessageEmbed()
  .setAuthor(`${member.user.tag}  ${member}`,`${member.user.displayAvatarURL()}`)
  .setFooter("Join")
  .setTimestamp()
  .setColor(0x2DB300);
  member.guild.channels.cache.get(channel.id).send({embed});
  member.guild.channels.cache.get(channel.id).send(ping);
};
