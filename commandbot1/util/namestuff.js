module.exports = async function (message, client) {
  const Discord = require("discord.js");
  const fetch = require("node-fetch");

  const skills = ['overall', 'attack', 'defence', 'strength', 'hitpoints', 'ranged', 'prayer', 'magic', 'cooking', 'woodcutting', 'fletching', 'fishing', 'firemaking', 'crafting', 'smithing', 'mining', 'herblore', 'agility', 'thieving', 'slayer', 'farming', 'runecraft', 'hunter', 'construction'];

  const emojiRegex = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/gi;


  const connection = client.connection;

  await message.guild.members.fetch();

  let logs = message.guild.channels.cache.get("439098363632287749");//rsn-logs CHANNEL
  if (message.content.length > 32) {
    message.guild.channels.cache.get(logs.id).send(`${message.author} + new name was too long \`\`\`${message.content}\`\`\``);
    message.delete();
    return;
  }
  if (message.content.includes("[") || message.content.includes("]")) {
    message.guild.channels.cache.get(logs.id).send(`${message.author} used [ or ] in their name \`\`\`${message.content}\`\`\` `);
    message.delete();
    return;
  }
  if (message.content.includes("<") || message.content.includes(">")) {
    message.guild.channels.cache.get(logs.id).send(`${message.author} used < or > in their name \`\`\`${message.content}\`\`\` `);
    message.delete();
    return;
  }
  if (message.content.includes("@")) {
    message.guild.channels.cache.get(logs.id).send(`${message.author} used @ in their name \`\`\`${message.content}\`\`\``);
    message.delete();
    return;
  }
  if (message.content.includes("#")) {
    message.guild.channels.cache.get(logs.id).send(`${message.author} used # in their name \`\`\`${message.content}\`\`\``);
    message.delete();
    return;
  }
  if(emojiRegex.test(message.content)){
    message.guild.channels.cache.get(logs.id).send(`${message.author} used an emoji in their name \`\`\`${message.content}\`\`\``);
    message.delete();
    return;
  }
  var names = parseRSN(message.content);

  //Dupe name check
  let namesInDisc = [];
  //put all names in array
  var members = message.guild.members.cache;
  for (var member of members) {
    try {
      var user = await message.guild.members.fetch(member[1].user.id);
    } catch (e) {
      console.log(member[1].user.id + " gave error " + e);
    }
    let roles = user.roles.cache;
    if (roles.has("408221074833145863")) {
      continue;
    }
    if (roles.has("800461880941346846")) {
      continue;
    }
    if (roles.has("442590109599268864")) {
      continue;
    }
    if (roles.size == 1) {
      continue;
    }
    var name = user.nickname;
    if (name == null || name == undefined) {//use discord name if no sever nickname
      name = member[1].user.username;
    }
    let parsedNames = parseRSN(name);
    for (var discName of parsedNames) {
      namesInDisc.push({"userid": user.id, "name": discName});
    }
  }

  for (var i = 0; i < names.length; i++) {
    let currentName = names[i].trim();
    if (currentName.length == 0)
      continue;
     if (message.member.roles.cache.has("442590109599268864")) {
       continue;
     }
    let namesMatched = namesInDisc.filter(el => el.name.toLowerCase() === currentName.toLowerCase());
    if (namesMatched.length > 0) {
      if (namesMatched.every(el => el.userid === message.author.id)) {
        continue;
      }
      namesMatched = namesMatched.filter(el => el.userid !== message.author.id);
      var pings = namesMatched.map(el => "<@!" + el.userid + "> ");
      pings += "<@!" + message.author.id + ">";
      var dupeChannel = message.guild.channels.cache.get("796438166135111730");
      dupeChannel.send(pings);
    }
  }

  for (var i = 0; i < names.length; i++) {
    message.guild.channels.cache.get(logs.id).send("!rw " + names[i]);
    var lookup = await getStats(names[i]);
    if (lookup.response == 200) {
      var sql_command_4 = "DELETE FROM WDR.USER_STATS WHERE rsn= ?";
      await connection.query(sql_command_4, names[i], function (error, result, fields) {
        if (error) {
          throw error
        }
      })
      var sql_command_5 = "INSERT INTO WDR.USER_STATS (rsn, stats, last_update) VALUES (? ,\'" + JSON.stringify(lookup.data) + "\',\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\')";
      await connection.query(sql_command_5, names[i], function (error, result, fields) {
        if (error) {
          throw error
        }
      })

      message.guild.channels.cache.get(logs.id).send(names[i] + " data saved");
    } else {
      if (lookup.response == 404) {
        const embed = new Discord.MessageEmbed()
          .setTitle('user not found on hiscore')
          .setColor(0xFF0000)
          .setDescription(names[i] + ' was not found on the hiscores');
        message.guild.channels.cache.get(logs.id).send({embed});
      } else {
        message.guild.channels.cache.get(logs.id).send(names[i] + " was not saved because hiscores were down (response code: " + lookup.response + ")");
      }

    }
  }
  //message.guild.channels.cache.get(logs.id).send("!update " + message.content);

  let nickname = message.content;

  let mentorId = "408107637100511243";
  let CoXMentorId = "520749698424766475";
  let ToBMentorId = "520750027405262858";
  let trialMentorId = "408246228204126209";

  let userroles = message.member.roles;
  if (userroles.cache.has(mentorId)) {
    if (userroles.cache.has(CoXMentorId) && userroles.cache.has(ToBMentorId)) {
      nickname += " [Mentor]";
    } else if (userroles.cache.has(CoXMentorId)) {
      nickname += " [CoX Mentor]";
    } else if (userroles.cache.has(ToBMentorId)) {
      nickname += " [ToB Mentor]";
    }
  } else if (userroles.cache.has(trialMentorId)) {
    nickname += " [Trial Mentor]"
  }

  message.member.setNickname(`${nickname}`)
    .then(message.author.send("Your name has been set to " + nickname))
    .catch(/*console.error*/);
  message.guild.channels.cache.get(logs.id).send(`${message.author} (${message.author.id}) + changed their name to ${nickname}`);
  message.delete()

  async function getStats(rsn) {
    const response = await fetch("https://secure.runescape.com/m=hiscore_oldschool/a=97/index_lite.ws?player=" + encodeURIComponent(rsn), {"redirect": "manual"});
    const text = await response.text();
    if (response.status == 200) {
      return ({"response": response.status, "data": parseStatsData(text)})
    } else {
      return ({"response": response.status, "data": null});
    }
  }

  function parseStatsData(data) {
    const skills = ['overall', 'attack', 'defence', 'strength', 'hitpoints', 'ranged', 'prayer', 'magic', 'cooking', 'woodcutting', 'fletching', 'fishing', 'firemaking', 'crafting', 'smithing', 'mining', 'herblore', 'agility', 'thieving', 'slayer', 'farming', 'runecraft', 'hunter', 'construction'];
    var sortedData = {};
    var splitData = data.split('\n');
    for (var i = 0; i < skills.length; i++) {
      var level = splitData[i].split(',')[1];
      var exp = splitData[i].split(',')[2];
      sortedData[skills[i]] = {"level": level, "exp": exp};
    }
    return sortedData;
  };

  function parseRSN(name) {
    return name.split("|").map(function (x) {
      var p_index = x.indexOf("(");
      if (p_index != -1) {
        x = x.substring(0, p_index);
      }
      var b_index = x.indexOf("[");
      if (b_index != -1) {
        x = x.substring(0, b_index);
      }
      return x.trim();
    })
  }
}
