module.exports = async function(client, message) {
  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well

  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  const google_sheet = google.sheets({version:"v4", auth:google_client})
  const opt= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "player_count"
  }

  var data = await google_sheet.spreadsheets.values.get(opt);
  var playercounts = data.data.values;

  var lastPlayerCount = playercounts[playercounts.length-1]

  var now = new Date();
  var dateStr = (now.getMonth()+1)+"/"+now.getDate()+"/"+now.getFullYear();

  if(lastPlayerCount[0] !== dateStr){
    console.log("Success");

    var currentPlayerCount = message.guild.memberCount;
    var Data = [];
    Data[0] = [];
    Data[0][0] = dateStr;
    Data[0][1] = currentPlayerCount;

    const opt= {
      spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
      range: "player_count!" + (playercounts.length+1)+":"+(playercounts.length+1),
      valueInputOption: "USER_ENTERED",
      resource: {values: Data}
    }


    var res = await google_sheet.spreadsheets.values.update(opt);
    return;


  }
}