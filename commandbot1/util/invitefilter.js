/*
returns false if all discord invite links are whitelisted
returns false if there are no discord invite links
returns true if there is a discord invite link and it is not whitelisted
*/

module.exports = function(message) {

  /* case insensitive (checked against match.toLowerCase()) */
  const vanity_whitelist = new Set([
    "wdr", // WDR
    "er", // Essence Running
    "gear", // Gear
    "ironscape", // Ironscape
    "oblivion", // Oblv
    "osrs", // Official OSRS
    "zuk", // Inferno speedrunning
  ]);

  /* case sensitive (checked against exact match) */
  const generated_whitelist = new Set([
    "gREZC7f", // WDR
    "8Rznqh5", // Essence Running
    "7KZHZ38", // Gear
    "GzNHnH2", // Hero PVM
    "Jtn5xSG", // Infinity Bingo
    "JNvajHZ", // Ironman Skilling Methods
    "KjK4ntq", // Megascale
    "U4QFHxP", // Oblv
    "DKeNJWA", // Pet Hunters
    "mePCs8U", // Runelite
    "pzhzFM", // Runewatch
    "qFWztcK", // Runewatch
    "e2effBN", // Skilling Methods
    "VfZsNEF", // Volc Mine
    "FXPUrUp", // Volc Mine
    "WKDNPEF", // Zalcano
    "eZqkbWE", // Zalcano
    "6yFQAuQ", // Nightmare info
    "69UtmMMCWP", // Solo CoX & CM
    "93t6Zph", // Inferno speedrunning
    "Hs6nSph", // CoX Speeds (leaked/igrow server)
  ]);

  let invite_codes = [];
  let invite_pattern = /discord(?:\.gg|app\.com\/invite|\.com\/invite)\/(\w+)/gi;

  let links = message.content.match(invite_pattern);

  let videoCallPattern = /(zoom\.us\/j\/[0-9]*)|(teams\.microsoft\.com\/l\/.*)|(webex\.com\/meet\/.*)/gi;
  let whatsappPattern = /((http(s)?:\/\/)?chat.whatsapp.com\/[-a-z0-9@:%._\+~#=])|((http(s)?:\/\/)?wa.me\/[0-9]{5,})/gi;
  let match = videoCallPattern.test(message.content) || whatsappPattern.test(message.content);



  if(match){
    return true;
  }

  if (!links) {
    return false;
  }

  for (let j = 0; j < links.length; j++) {
    let link_parts = links[j].split('/');
    invite_codes.push(link_parts[link_parts.length - 1]);
  }

  for (let i = 0; i < invite_codes.length; i++) {
    if (!generated_whitelist.has(invite_codes[i]) && !vanity_whitelist.has(invite_codes[i].toLowerCase())) {
      return true;
    }
  }
  return false;
}
